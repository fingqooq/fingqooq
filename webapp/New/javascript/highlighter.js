var $$ = {};
var article;
var navbar = document.querySelector('.navbar');

window.addEventListener('load', function (e) {
    var hl = new highlighter('#F6F94A');
    article = $$.getById('fingqooq-article');
    saveHLBtn = $$.getById('fingqooq-saveHL');

    // hl.reLoad(location.pathname.split('/')[location.pathname.split('/').length-1]);

    article.addEventListener('mousedown', hl.getEvent, false);
    article.addEventListener('mousemove', hl.getEvent, false);
    article.addEventListener('mouseup', hl.getEvent, false);

    document.addEventListener('scroll', scroll, false);

    navbar.addEventListener('mouseover', navbarover, false);
    // saveHLBtn.addEventListener('click', hl.save, false);
}, false);

var prevScroll;

function scroll (ev) {
    var body = document.querySelector('body');

    if (!prevScroll) {
        prevScroll  = body.scrollTop;
    };

    scroll = body.scrollTop;

    if (scroll > 50 && scroll >= prevScroll+10) {
        $$.addClass(navbar, 'opecity-0');
    } else if (scroll > 50 && scroll < prevScroll) {
        $$.removeClass(navbar, 'opecity-0');
    } else if (scroll < 50) {
        $$.removeClass(navbar, 'opecity-0');
    }

    prevScroll = scroll;
};

function navbarover (ev) {
    $$.removeClass(navbar, 'opecity-0');
}


var highlighter = function (colour) {
    var hl = this;
    var startX, startY, endX, endY;
    var sel, rectTop;
    this.forward = false;
    this.started = false;

    this.getEvent = function (ev) {
        var func = hl[ev.type];
        if(func) {
            func(ev);
        }
    };

    this.mousedown = function (ev) {
        console.log("mousedown");
        startX = ev.x;
        startY = ev.y;
        hl.started = true;
    };

    this.mousemove = function (ev) {
        if (hl.started) {
            var range, rect;
            if(!rectTop) {
                sel = window.getSelection();
                range = sel.getRangeAt(0);
                rect = range.getClientRects()[0];
                rectTop = rect.top;
            }
            console.log("mousemove to x : "+ ev.x + " y : " + ev.y + " rectTop : " + rectTop);
            
            endX = ev.x;
            endY = ev.y;

            if (startX < endX) {
                if (rectTop-18 < endY) {
                    hl.mouseforward();
                } else{
                    hl.mousebackward();
                };
            } else {
                if (rectTop+18 < endY) {
                    hl.mouseforward();
                } else{
                    hl.mousebackward();
                };
            };
        };

    }

    this.mouseup = function (ev) {
        console.log("mouseup");

        if (hl.forward) {
            document.designMode = "on"; 
            document.execCommand("BackColor", false, colour);
            document.designMode = "off";
        } else{
            document.designMode = "on"; 
            document.execCommand("BackColor", false, 'transparent');
            document.designMode = "off";
        };

        sel = window.getSelection();
        sel.removeAllRanges();

        $$.removeClass(article, 'mousebackward');
        $$.removeClass(article, 'mouseforward');
        rectTop = null;
        hl.started = false;
        $$.removeClass(navbar, 'opecity-0');
    };

    this.mouseforward = function () {
        hl.forward = true;
        $$.removeClass(article, 'mousebackward');
        $$.addClass(article, 'mouseforward');
    };

    this.mousebackward = function () {
        hl.forward = false;
        $$.removeClass(article, 'mouseforward');
        $$.addClass(article, 'mousebackward');
    };

    this.save = function () {
        var serializedString = hl.serialize();

        console.log(serializedString);

        $$.ajax ({
            method : 'POST',
            url : '../api/v1/highlight',
            async : true,
            content : 'application/json',
            sendData : serializedString,
            success : function (data) {
                console.log("Save Success");
            }
        });
    }

    this.reload = function (articleId) {
        $$.ajax ({
            method : 'GET',
            url : '../api/v1/highlight?article_id='+articleId,
            async : true,
            success : function (data) {
//                var fixedData = $.unicodeDecoder(data);
                hl.deserialize(JSON.parse(data));
            }
        });
    }

    this.serialize = function () {
        var oData = {};
        var aSerialized = [];
        article.innerHTML = article.innerHTML.replace(/[\f\n\r\t\v]/g,"");
        var highlights = document.querySelectorAll("span[style^='background-color: rgb']");
        for (var i = 0, length = highlights.length; i < length; i++) {
            var oEle = {};
            var ele = highlights[i];
            var temp = hl.getTempSpan();
            ele.parentNode.insertBefore(temp, ele);

            oEle.color = ele.style.backgroundColor;
            oEle.start = article.textContent.indexOf('@#$@#$!!');
            oEle.text = [];

            for (var k = 0; k < ele.childNodes.length; k++) {
                var textContent = ele.childNodes[k].textContent;
                var aTextContent = textContent.split(/[\f\n\r\t\v]/g);
                for (var j = 0; j < aTextContent.length; j++) {
                    if(aTextContent[j].length >= 1) {
                        oEle.text.push(aTextContent[j]);
                    } else {
                        if (k == 0) {
                            oEle.start++;
                        };
                    }
                };

            };

            aSerialized.push(oEle);
            // Remove temp span.
            document.querySelector('.fingqooq-hidden').remove();
        };

        oData.article_id = location.pathname.split('/')[location.pathname.split('/').length-1];
        oData.data = aSerialized;

        return JSON.stringify(oData);
    };

    this.deserialize = function (aSerialized) {
        article.innerHTML = article.innerHTML.replace(/[\f\n\r\t\v]/g,"");
        for (var i = 0; i < aSerialized.length; i++) {
            var oEle = aSerialized[i];
            var pos = 0, count = 0;

            while(oEle.start != article.textContent.indexOf(oEle.text[0], pos)) {
                pos = article.textContent.indexOf(oEle.text[0], pos)+1;
                count++;
            };

            pos = 0;
            for (var j = 0; j < count; j++) {
                pos = article.innerHTML.indexOf(oEle.text[0], pos);
                pos++;
            };

            for (var k = 0; k < oEle.text.length; k++) {
                var text = oEle.text[k];
                article.innerHTML = article.innerHTML.substring(0, pos)
                                    + article.innerHTML.substring(pos).replace(text, "<span style='background-color: " + oEle.color + "'>" + text + "</span>");
            };
        };
    };

    this.getTempSpan = function () {
        var tempspan = document.createElement('span');
        tempspan.textContent = "@#$@#$!!";
        $$.addClass(tempspan, 'fingqooq-hidden');

        return tempspan;
    };
}




function makeEditableAndHighlight (colour) {
    var range, sel = window.getSelection();
    if (sel.rangeCount && sel.getRangeAt) {
        range = sel.getRangeAt(0);
    }
    document.designMode = "on";
    if (range) {
        sel.removeAllRanges();
        sel.addRange(range);
    }
    // Use HiliteColor since some browsers apply BackColor to the whole block
    if (!document.execCommand("HiliteColor", false, colour)) {
        document.execCommand("BackColor", false, colour);
    };

    sel.removeAllRanges();
    document.designMode = "off";
};

function highlightSelection (colour) {
    var range, sel;
    if (window.getSelection) {
        // IE9 and non-IE
        try {
            if (!document.execCommand("BackColor", false, colour)) {
                makeEditableAndHiight(colour);
            }
        } catch (ex) {
            makeEditableAndHighlight(colour)
        }
    } else if (document.selection && document.selection.createRange) {
        // IE <= 8 case
        range = document.selection.createRange();
        range.execCommand("BackColor", false, colour);
    }
};

// Element를 찾아서 DOM을 반환하는 함수.
// param : name
$$.getByName = function (param) {
    return document.getElementsByName(param)[0];
};

// Element를 찾아서 DOM을 반환하는 함수.
// param : id
$$.getById = function (param) {
    return document.getElementById(param);
};

// Info Object를 받아 ajax요청을 하는 함수, 내부에서 함수를 실행하거나 리턴함수로 넘겨줌
// oInfo = {
//  method : 요청 method(String),
//  url : 요청 url(String),
//  async : 비동기 - true, 동기 - false,
//  content : Content type,
//  sendData : 보낼 데이터, 기본값 null,
//  success : 성공시 실행되는 함수,
//  error : 에러시 실행되는 함수
// }
$$.ajax = function (oInfo) {
    var xhr, sendData;
    // code for IE7+, Firefox, Chrome, Opera, Safari
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    // code for IE6, IE5
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    };

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var responseText = xhr.responseText;
            oInfo.success(responseText);
        } else {
            if (oInfo.error) {
                oInfo.error();
            };
        }
    };

    xhr.open(oInfo.method, oInfo.url, oInfo.async);
    xhr.setRequestHeader("Content-Type", oInfo.content);

    sendData = oInfo.sendData || null;
    xhr.send(sendData);
};

// Dom의 class를 toggle하는 함수
$$.toggleClass = function (dom, className) {
    if (this.hasClass(dom, className)) {
        dom.classList.remove(className);
    } else {
        dom.classList.add(className);
    };
};

// Dom의 class가 없으면 추가하는 함수
$$.addClass = function (dom, className) {
    dom.classList.add(className);
};

// Dom의 class가 있으면 빼는 함수
$$.removeClass = function (dom, className) {
    dom.classList.remove(className);
};

// Dom이 className이 있으면 true, 없으면 false를 반환하는 함수
$$.hasClass = function (dom, className) {
    for (var i = 0; i < dom.classList.length; i++) {
        if (dom.classList[i] == className) {
            return true;
        };
    };

    return false;

$$.unicodeDecoder = function(str) {
    str=str.replace("u0001","âº");
    str=str.replace("u0002","â»");
    str=str.replace("u0003","â¥");
    str=str.replace("u0004","â¦");
    str=str.replace("u0005","â£");
    str=str.replace("u0006","â ");
    str=str.replace("u0007","â¢");
    str=str.replace("u0008","â");
    str=str.replace("u0009","â");
    str=str.replace("u000A","â");
    str=str.replace("u000B","â");
    str=str.replace("u000C","â");
    str=str.replace("u000D","âª");
    str=str.replace("u000E","â«");
    str=str.replace("u000F","â¼");
    str=str.replace("u0010","âº");
    str=str.replace("u0011","â");
    str=str.replace("u0012","â");
    str=str.replace("u0013","â¼");
    str=str.replace("u0014","Â¶");
    str=str.replace("u0015","Â§");
    str=str.replace("u0016","?");
    str=str.replace("u0017","?");
    str=str.replace("u0018","â");
    str=str.replace("u0019","â");
    str=str.replace("u001A","â");
    str=str.replace("u001B","â");
    str=str.replace("u001C","â");
    str=str.replace("u001D","â");
    str=str.replace("u001E","â²");
    str=str.replace("u001F","â¼");
    str=str.replace("u0020"," ");
    str=str.replace("u0021","!");
    str=str.replace("u0022","\"");
    str=str.replace("u0023","#");
    str=str.replace("u0024","$");
    str=str.replace("u0025","%");
    str=str.replace("u0026","&");
    str=str.replace("u0027","'");
    str=str.replace("u0028","(");
    str=str.replace("u0029",")");
    str=str.replace("u002A","*");
    str=str.replace("u002B","+");
    str=str.replace("u002C",",");
    str=str.replace("u002D","-");
    str=str.replace("u002E",".");
    str=str.replace("u2026","â¦");
    str=str.replace("u002F","/");
    str=str.replace("u0030","0");
    str=str.replace("u0031","1");
    str=str.replace("u0032","2");
    str=str.replace("u0033","3");
    str=str.replace("u0034","4");
    str=str.replace("u0035","5");
    str=str.replace("u0036","6");
    str=str.replace("u0037","7");
    str=str.replace("u0038","8");
    str=str.replace("u0039","9");
    str=str.replace("u003A",":");
    str=str.replace("u003B",";");
    str=str.replace("u003C","<");
    str=str.replace("u003D","=");
    str=str.replace("u003E",">");
    str=str.replace("u2264","â¤");
    str=str.replace("u2265","â¥");
    str=str.replace("u003F","?");
    str=str.replace("u0040","@");
    str=str.replace("u0041","A");
    str=str.replace("u0042","B");
    str=str.replace("u0043","C");
    str=str.replace("u0044","D");
    str=str.replace("u0045","E");
    str=str.replace("u0046","F");
    str=str.replace("u0047","G");
    str=str.replace("u0048","H");
    str=str.replace("u0049","I");
    str=str.replace("u004A","J");
    str=str.replace("u004B","K");
    str=str.replace("u004C","L");
    str=str.replace("u004D","M");
    str=str.replace("u004E","N");
    str=str.replace("u004F","O");
    str=str.replace("u0050","P");
    str=str.replace("u0051","Q");
    str=str.replace("u0052","R");
    str=str.replace("u0053","S");
    str=str.replace("u0054","T");
    str=str.replace("u0055","U");
    str=str.replace("u0056","V");
    str=str.replace("u0057","W");
    str=str.replace("u0058","X");
    str=str.replace("u0059","Y");
    str=str.replace("u005A","Z");
    str=str.replace("u005B","[");
    str=str.replace("u005C","\\");
    str=str.replace("u005D","]");
    str=str.replace("u005E","^");
    str=str.replace("u005F","_");
    str=str.replace("u0060","`");
    str=str.replace("u0061","a");
    str=str.replace("u0062","b");
    str=str.replace("u0063","c");
    str=str.replace("u0064","d");
    str=str.replace("u0065","e");
    str=str.replace("u0066","f");
    str=str.replace("u0067","g");
    str=str.replace("u0068","h");
    str=str.replace("u0069","i");
    str=str.replace("u006A","j");
    str=str.replace("u006B","k");
    str=str.replace("u006C","l");
    str=str.replace("u006D","m");
    str=str.replace("u006E","n");
    str=str.replace("u006F","o");
    str=str.replace("u0070","p");
    str=str.replace("u0071","q");
    str=str.replace("u0072","r");
    str=str.replace("u0073","s");
    str=str.replace("u0074","t");
    str=str.replace("u0075","u");
    str=str.replace("u0076","v");
    str=str.replace("u0077","w");
    str=str.replace("u0078","x");
    str=str.replace("u0079","y");
    str=str.replace("u007A","z");
    str=str.replace("u007B","{");
    str=str.replace("u007C","|");
    str=str.replace("u007D","}");
    str=str.replace("u02DC","Ë");
    str=str.replace("u007E","â¼");
    str=str.replace("u007F","");
    str=str.replace("u00A2","Â¢");
    str=str.replace("u00A3","Â£");
    str=str.replace("u00A4","Â¤");
    str=str.replace("u20AC","â¬");
    str=str.replace("u00A5","Â¥");
    str=str.replace("u0026quot;","\"");
    str=str.replace("u0026gt;",">");
    str=str.replace("u0026lt;",">");
    return str;
};
};
