<%--
  Created by IntelliJ IDEA.
  User: WooGenius
  Date: 5/19/14
  Time: 1:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
    <meta id="viewport" name="viewport"
          content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>
        ${article.title}
    </title>
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/article.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <style>
        body {
            padding-top: 50px;
        }
    </style>
    <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-52420606-1', 'auto');
ga('send', 'pageview');

	</script>
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top fadeout">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand c-red" href="../articles">FINGQOOQ</a>
            </div>
            <div class="navbar-collapse collapse navbar-inverse-collapse">
                <ul class="nav navbar-nav">
                    <c:if test="${session}"><li><a id="btnMyArticles" href = ${myarticles}>MY ARTICLES</a></li></c:if>
                    <li><a id="btnSaveHighlight" href>SAVE HIGHLIGHT</a></li>
                    <li><a id="btnResetHighlight" href>RESET HIGHLIGHT</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                	<c:if test="${session}"><li><a id="btnLogout" href = "/LogoutUser">LOGOUT</a></li></c:if>
                	<c:if test="${session == false }"><li><a id="btnLogin" href = "/LoginWithFB">LOGIN with FB</a></li></c:if>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="fingqooq-article">
                <h1>
                    ${article.title}
                </h1>
                <div id="fingqooq-url">
                    <a href="${article.url}">Read Original Text</a>
                </div>
                ${article.contents}
        </div>
    </div>

    <div class="fingqooq-footer">
        <a href="#" id="fingqooq-read-btn" data-toggle="tooltip" data-placement="top" title="Didn't you read?">
            <img src="../img/checked.png">
            <img src="../img/unchecked.png" class="fadeout" id="fingqooq-uncheckimg">
        </a>
        <div id="fingqooq-readOrUnread">
            <div id="fingqooq-read">Read</div>
            <div id="fingqooq-unread">Unread</div>
        </div>
        You read <span id="fingqooq-readCount" class="c-red">${article.read}</span> articles.
    </div>

    <div id="fb-root"></div>
    <script src="../javascript/jquery-1.11.1.min.js"></script>
    <script src="../javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="../javascript/highlighter.js"></script>
    <script type="text/javascript">
        var elReadBtn = document.getElementById('fingqooq-read-btn');
        var footer = document.querySelector('.fingqooq-footer');
        var body = document.querySelector('body');
        var minScrollTop = 1000000;
        var read = false;

        elReadBtn.addEventListener('click', function (e) {
            e.preventDefault();
            if (read) {
                unreadArticle();
                minScrollTop = body.scrollTop;
                read = false;
            };
        }, false);

        document.addEventListener('scroll', function (e) {
            var height = $(window).height();
            var scrollTop = body.scrollTop;

            if (scrollTop < minScrollTop) {
                minScrollTop = scrollTop;
            };

            if (!read && minScrollTop <= 50) {
                if ((height+body.scrollTop) > (body.scrollHeight-125)) {
                    read = true;
                    readArticle();
                };
            };
        }, false);

        function readArticle () {
            var aid = location.pathname.split('/')[2];
            var oAjaxParam = {
                callback 	: function() {
                    var responseStr = xhr.responseText;
                    var jsonObj = JSON.parse(responseStr);
                    if (jsonObj.code == 200) {
                        setTimeout (function () {
                            $('#fingqooq-read-btn').tooltip('show');
                            $("#fingqooq-uncheckimg").addClass("opacity-0");
                            $("#fingqooq-unread").addClass("opacity-0");
                            $("#fingqooq-readCount").fadeOut(function() {
                                $(this).text(jsonObj.read).fadeIn();
                            });
                        }, 500);
                    } else {
                        alret("Try Again!");
                    }
                },
                method 		: 'GET',
                url			: '/article/read?article-id='+aid,
                async		: false,
                contentType : 'application/json',
                form 		: null
            };
            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    oAjaxParam.callback(xhr);
                }
            };

            xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
            xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
            xhr.send(oAjaxParam.form);
        }

        function unreadArticle () {
            var aid = location.pathname.split('/')[2];
            var oAjaxParam = {
                callback 	: function() {
                    var responseStr = xhr.responseText;
                    var jsonObj = JSON.parse(responseStr);
                    if (jsonObj.code == 200) {
                        $('#fingqooq-read-btn').tooltip('destroy');
                        $("#fingqooq-uncheckimg").removeClass("opacity-0");
                        $("#fingqooq-unread").removeClass("opacity-0");
                        $("#fingqooq-readCount").fadeOut(function() {
                            $(this).text(jsonObj.read).fadeIn();
                        });
                    } else {
                        alret("Try Again!");
                    }
                },
                method 		: 'GET',
                url			: '/article/unread?article-id='+aid,
                async		: false,
                contentType : 'application/json',
                form 		: null
            };
            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    oAjaxParam.callback(xhr);
                }
            };

            xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
            xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
            xhr.send(oAjaxParam.form);
        }
    </script>
    <c:if test="${article.didRead == 1}">
        <script type="text/javascript">
            read = true;
            $('#fingqooq-read-btn').tooltip('show');
            $("#fingqooq-uncheckimg").addClass("opacity-0");
            $("#fingqooq-unread").addClass("opacity-0");
        </script>
    </c:if>
</body>
</html>
