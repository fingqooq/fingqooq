var MAIN_PAGE_PAGING = (function() {
        	function paging() {
            	var nViewportHeight = window.innerHeight;
            	var nScroll = document.body.scrollTop;
            	var nOffsetHeight = document.body.offsetHeight;
            	if (nViewportHeight + nScroll >= nOffsetHeight) {
            		var nCurrentPage = parseInt(document.getElementById('scroll-check').value);
            		var oAjaxParam = {
                            callback 	: function() {
                                var responseStr = xhr.responseText;
                                var jsonObj = JSON.parse(responseStr);
                                var elTemplate = document.querySelector('.col-sm-6');
                                var elArticlesRow = document.querySelector('.main-articles');
                                var aNewArticle = [];
                                for (var i in jsonObj) {
    	                            console.log(jsonObj[i].didQooq);
                                	aNewArticle.push(elTemplate.cloneNode(true));
                                	aNewArticle[i].querySelector('.panel-body > a').href = './article/' + jsonObj[i].id;
                                	aNewArticle[i].querySelector('.article-title').innerHTML = jsonObj[i].title;
                                	aNewArticle[i].querySelector('.read-count').innerHTML = jsonObj[i].read;
                                	aNewArticle[i].querySelector('.qooq-count').innerHTML = jsonObj[i].qooq;
                                	var elBtn = aNewArticle[i].querySelector('.qooq-btn');
                                	elBtn.setAttribute('data-aid', jsonObj[i].id);
                                	if (jsonObj[i].didQooq == 1) {
                                		elBtn.innerHTML = 'Q';
                                		elBtn.classList.add('unqooq-status');
                                	} else {
                                		elBtn.innerHTML = 'Q';
                                		elBtn.classList.remove('unqooq-status');
                                	}
                                	elArticlesRow.appendChild(aNewArticle[i]);
                                }
                                
                                if (jsonObj.length < 10) {
                                	window.removeEventListener("scroll", paging);
                                }
                                
                                var elScrollCheck = document.getElementById('scroll-check');
                                elScrollCheck.value = parseInt(elScrollCheck.value) + 1;
                            },
                            method 		: 'GET',
                            url			: '/nextarticle?current-page=' + (nCurrentPage + 1),
                            async		: false,
                            contentType : 'application/json',
                            form 		: null
                        };
                        var xhr = new XMLHttpRequest();

                        xhr.onreadystatechange = function() {
                            if (xhr.readyState === 4 && xhr.status === 200) {
                                oAjaxParam.callback(xhr);
                            }
                        };

                        xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
                        xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
                        xhr.send(oAjaxParam.form);
            	}
          	}
            document.addEventListener("DOMContentLoaded", function(e) {
            	document.getElementById('scroll-check').value = 0;
            });
            window.addEventListener("scroll", paging);
        }());