var DragDrop = $Class({
	$init : function(sDragClass, sDropId) {
		// drag compononet
		this.aDrag = $$('.'+sDragClass);
		
		// selected element
		this.elDrag;
		
		// element of drop area
		this.welDrop = $Element(sDropId);
		
		// object of position value in the area
		this.diff = { X : 0, Y : 0};
		
		// event handler
		this.oEvent = {};
		
		// flag for drag status
		this.bDragging = false;
		
		// clone for drag element
		this.welementClone;
		
		// flag for drop status
		this.bDropped = false;
		
		// object for position of drop area
		this.dropArea = {};
		
		// Internet explorer
		this.bIE = $Agent().navigator().ie;
		
		
		this._attachEvent();
	},
	
	// get position value of drop area
	_setDropDimension : function() {
		var oOffset = this.welDrop.offset();
		
		this.dropArea = {
			top : oOffset.top,
			left : oOffset.left
		};
		
		this.dropArea.bottom = this.dropArea.top + this.welDrop.height();
		this.dropArea.right = this.dropArea.left + this.welDrop.width();
	},
	
	// register onmousedown event
	_attachEvent : function() {
		for (var i in this.aDrag) {
			$Fn(function(oEvent) {
				this._setDropDimension();
				$Fn(this._setDropDimension, this).attach(window, "resize");
				var el = oEvent.element;
				this.elDrag = el;
				var oMouse = oEvent.mouse();
				
				// check click button (right or left)
				// operate on left click
				if(!oMouse.left) {
					return;
				}
				
				// position of event fired
				var oDownPosition = oEvent.pos();
				
				// location of draggable element
				var oOffset = $Element(el).offset();
				
				// get precise position value of cursor clicked
				this.diff.Y = oDownPosition.pageY - oOffset.top;
				this.diff.X = oDownPosition.pageX - oOffset.left;
				
				// register mousemove event handler
				this._setMouseEvent(true);
			}, this).attach(this.aDrag[i], "mousedown");
		}
	},
	
	// (un)register onmousemove/onmouseup event handler
	_setMouseEvent : function(bAttach) {
		//register event
		if (bAttach) {
			
			// capture mouse event on IE
			if(this.bIE) {
				document.body.setCapture();
			}
			
			this.oEvent.mouseMove = $Fn(this._mouseMove, this).attach(document, "mousemove");
			this.oEvent.mouseUp = $Fn(this._mouseUp, this).attach(document, "mouseup");
		}
		// unregister event
		else {
			// uncapture mouse event on IE
			if (this.bIE) {
				document.body.releaseCapture();
			}
			
			this.oEvent.mouseMove.detach(document, "mousemove");
			this.oEvent.mouseUp.detach(document, "mouseup");
		}
	},
	
	// onmousemove event handler
	_mouseMove : function(oEvent) {
		// get position value of event fired
		var oPosition = oEvent.pos();
		
		// if mousemove handler operates first time,
		// set position value of drag element to 'absolute'
		// set drag flag to 'true'
		if (!this.bDragging) {
			// if cloned element doesn't exist, create clone element and add to DOM
			if (!this.welementClone) {
				this.welementClone = $Element("<div id='_tmpDrag' class='drag'>");
				document.body.appendChild(this.welementClone.$value());
				//this.welementClone.html(this.elDrag.innerHTML);
				
				// set half-transparency style
				this.welementClone.opacity(0.4);
			}
			// if clone element already exists in DOM, just use it
			else {
				this.welementClone = $Element("_tmpDrag");
				this.welementClone.show();
			}
			
			this.welementClone.css({'position' : 'absolute',
									'width'    : '30px',
									'height'   : '30px',
									'border'   : '1px solid black'});
			this.bDragging = true;
		}
		
		// change position of drag element based on event fired
//		this.welementClone.css({
//			top : (oPosition.pageY - this.diff.Y) + "px",
//			left : (oPosition.pageX - this.diff.X) + "px"
//		});
		this.welementClone.css({
			top : (oPosition.pageY) + "px",
			left : (oPosition.pageX) + "px"
		});
		
		console.log("oPosition X :" + oPosition.clientX + ", Y : " + oPosition.clientY);
		console.log("dropArea.top :" + this.dropArea.top + ", bottom : " + this.dropArea.bottom + ", left : " + this.dropArea.left + ", right : " + this.dropArea.right);
		
		// if cursor is in the drop area,
		// change background color of drop area and
		// set bDragging to 'true'
		if (oPosition.pageY > this.dropArea.top &&
			oPosition.pageY < this.dropArea.bottom &&
			oPosition.pageX > this.dropArea.left &&
			oPosition.pageX < this.dropArea.right) {
			this.welDrop.css("backgroundColor", "red");
			this.bDropped = true;
		}
		// if no drop area, change the background color of drop area to white,
		// and set bDragging to 'false'
		else {
			this.welDrop.css("backgroundColor", "white");
			this.bDropped = false;
		}
	},
	
	// onmouseup event handler
	_mouseUp : function(oEvent) {
		var nArticleId = this._getArticleId();
		
		// because drag ends, set bDragging to 'false'
		this.bDragging = false;
		
		// unregister registered event
		this._setMouseEvent(false);
		
		if (this.bDropped) {
			var oAjaxParam = {
					callback 	: function() {
									var responseStr = xhr.responseText;
									if (responseStr == "SUCCESS") {
										alert("QooQed!!")
									} else {
										alret("Try Again!");
									}
								},
					method 		: 'GET',
					url			: '/qooq?id=' + nArticleId,
					async		: false,
					contentType : 'application/json',
					form 		: null
			};
			
			var xhr = new XMLHttpRequest();
				
			xhr.onreadystatechange = function() {
				if (xhr.readyState === 4 && xhr.status === 200) {
					oAjaxParam.callback(xhr);
				} 
			};
				
			xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
			xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
			xhr.send(oAjaxParam.form);
		}
		
		// set position value of original element to value of clone element
		if (this.welementClone) {
			/*
			$Element(this.elDrag).css({
				position : "absolute",
				top : this.welementClone.css("top"),
				left : this.welementClone.css("left")
			});
			*/
			// hide clone element
			this.welementClone.hide();
			this.welDrop.css("backgroundColor", "white");
		}
		
		this.elDrag = null;
	},
	
	_getArticleId : function() {
		var sUrl = this.elDrag.parentElement.getAttribute('href');
		
		return sUrl.split('/')[2];
	}
});