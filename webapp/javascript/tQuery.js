function $$$(selector, targetElement) {
	var returnElem = null;
	if (targetElement === undefined) {
		returnElem = document.querySelectorAll(selector);
	} else {
		returnElem = targetElement.querySelectorAll(selector);
	}
	
	if (returnElem !== null && returnElem.length == 1) {
		returnElem = returnElem[0];
	}
	
	return returnElem;
}

		
/**
 * description : Ajax를 등록하는 함수
 * param : oAjaxParam (ajax 관련 파라미터를 모아놓은 객체)
 * var oAjaxParam = {
 * 			callback 	: ajax 요청이 제대로 전송되었을 경우 실행될 콜백 함수,
 * 			method 	 	: GET or POST,
 * 			url 	 	: ajax 요청을 보낼 url,
 * 			async	 	: ajax 요청을 동기(false) 로 보낼지 비동기(true)로 보낼지 결정,
 * 			contentType : ajax 요청을 보내는 헤더,
 * 			form		: ajax 요청과 함께 보낼 객체 (form 데이터)
 * 		}
 */
function callAJAX(oAjaxParam) {
	var xhr = new XMLHttpRequest();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status === 200) {
			oAjaxParam.callback(xhr);
		} 
	};
	
	xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
	xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
	xhr.send(oAjaxParam.form);
}

/**
 * Event 관련 util 함수
 */
var EventUtil = {
	
	addHandler: function(element, type, handler) {
		// modern browsers : chrome, safari, firefox
		if (element.addEventListener) {
			element.addEventListener(type, handler, false);
		}
		// Internet Explorer
		 else if (element.attachEvent) {
			element.attachEvent("on" + type, handler);
		}
		// Old Browsers
		 else {
			element["on" + type] = handler;
		}
	},
	
	removeHandler: function(element, type, handler) {
		// modern browsers : chrome, safari, firefox
		if (element.removeEventListener) {
			element.removeEventListener(type, handler, false);
		}
		// Internet Explorer
		 else if (element.detachEvent) {
			element.detachEvent("on" + type, handler);
		}
		// Old Browsers
		 else {
			element["on" + type] = null;
		}
	},
	
	getEvent: function(event) {
		return event ? event : window.event;
	},
	
	getTarget: function(event) {
		return event.target || event.srcElement;
	},
	
	preventDefault: function(event) {
		// DOM Standard
		if (event.preventDefault) {
			event.preventDefault();
		}
		// Internet Explorer
		else {
			event.returnValue = false;
		}
	},
	
	stopPropagation: function(event) {
		// DOM Standard
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		// Internet Explorer
		else {
			event.cancelBubble = true;
		}
	},
	// Only for mouseout, mouseover
	getRelatedTarget: function(event) {
		// DOM Standard
		if (event.relatedTarget) {
			return event.relatedTarget;
		} 
		// mouseout; IE 8 and earlier
		// relatedTarget : destination
		else if (event.toElement) {
			return event.toElement;
		}
		// mouseover; IE 8 and earlier
		// relatedTarget : departure
		else if (event.fromElement) {
			return event.fromElement;
		}
		else {
			return null;
		}
	},
	
	getButton: function(event) {
		// DOM Standard
		if (document.implementation.hasFeature("MouseEvents", "2.0")) {
			return event.button;
		}
		// Internet Explorer
		else {
			switch(event.button) {
				case 0:
				case 1:
				case 3:
				case 5:
				case 7:
					return 0;
				case 2:
				case 6:
					return 2;
				case 4:
					return 1;
			}
		}
	},
	
	getWheelDelta: function(event) {
		if (event.wheelDelta) {
			// Opera has different wheel delta value
			return (client.engine.opera && client.engine.opera < 9.5 ?
					-event.wheelDelta : event.wheelDelta);
		} else {
			return -event.detail * 40;
		}
	},
	
	// 'keypress' event
	getCharCode: function(event) {
		if (typeof event.charCode == "number") {
			return event.charCode;
		}
		// character pressed
		else {
			return event.keyCode;
		}
	}
};

/**
 * 'Date' object expanded
 */

//For todays date;
Date.prototype.today = function () { 
    return this.getFullYear() + "-" + ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1);
};

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +"-"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +"-"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
};