var $$ = {};
var article = document.querySelector('#fingqooq-article');
var navbar = document.querySelector('.navbar');

window.addEventListener('load', function (e) {
    article = $$.getById('fingqooq-article');
    btnSaveHighlight = $$.getById('btnSaveHighlight');
    btnResetHighlight = $$.getById('btnResetHighlight');
    btnFingQooq = $$.getById('btnFingQooq');

    article.addEventListener('mousedown', hl.getEvent, false);
    article.addEventListener('mousemove', hl.getEvent, false);
    article.addEventListener('mouseup', hl.getEvent, false);

    document.addEventListener('scroll', scroll, false);

    navbar.addEventListener('mouseover', navbarover, false);

    btnSaveHighlight.addEventListener('click', hl.save, false);
    btnResetHighlight.addEventListener('click', hl.reset, false);

}, false);

var prevScroll;

function scroll (ev) {
    var body = document.querySelector('body');

    if (!prevScroll) {
        prevScroll  = body.scrollTop;
    };

    scroll = body.scrollTop;

    if (scroll > 50 && scroll >= prevScroll+10) {
        $$.addClass(navbar, 'opacity-0');
    } else if (scroll > 50 && scroll < prevScroll) {
        $$.removeClass(navbar, 'opacity-0');
    } else if (scroll < 50) {
        $$.removeClass(navbar, 'opacity-0');
    }

    prevScroll = scroll;
};

function navbarover (ev) {
    $$.removeClass(navbar, 'opacity-0');
}


var highlighter = function (colour) {
    var hl = this;
    var startX, startY, endX, endY;
    var sel, rectTop;
    this.forward = false;
    this.started = false;

    this.getEvent = function (ev) {
        var func = hl[ev.type];
        if(func) {
            func(ev);
        }
    };

    this.mousedown = function (ev) {
        console.log("mousedown");
        startX = ev.x;
        startY = ev.y;
        hl.started = true;
    };

    this.mousemove = function (ev) {
        if (hl.started) {
            var range, rect;
            if(!rectTop) {
                sel = window.getSelection();
                range = sel.getRangeAt(0);
                rect = range.getClientRects()[0];
                rectTop = rect.top;
            }
            console.log("mousemove to x : "+ ev.x + " y : " + ev.y + " rectTop : " + rectTop);
            
            endX = ev.x;
            endY = ev.y;

            if (startX < endX) {
                if (rectTop-18 < endY) {
                    hl.mouseforward();
                } else{
                    hl.mousebackward();
                };
            } else {
                if (rectTop+18 < endY) {
                    hl.mouseforward();
                } else{
                    hl.mousebackward();
                };
            };
        };

    }

    this.mouseup = function (ev) {
        console.log("mouseup");

        if (hl.forward) {
            document.designMode = "on"; 
            document.execCommand("HiliteColor", false, colour);
            document.designMode = "off";
        } else{
            document.designMode = "on"; 
            document.execCommand("HiliteColor", false, 'transparent');
            document.designMode = "off";
        };

        sel = window.getSelection();
        sel.removeAllRanges();

        $$.removeClass(article, 'mousebackward');
        $$.removeClass(article, 'mouseforward');
        rectTop = null;
        hl.started = false;
        $$.removeClass(navbar, 'opacity-0');
    };

    this.mouseforward = function () {
        hl.forward = true;
        $$.removeClass(article, 'mousebackward');
        $$.addClass(article, 'mouseforward');
    };

    this.mousebackward = function () {
        hl.forward = false;
        $$.removeClass(article, 'mouseforward');
        $$.addClass(article, 'mousebackward');
    };

    this.save = function (ev) {
        ev.preventDefault();
        var serializedString = hl.serialize();

        console.log(serializedString);

        $$.ajax ({
            method : 'POST',
            url : '../api/v1/highlight',
            async : true,
            content : 'application/json',
            sendData : serializedString,
            success : function (data) {
            	var responseStr = data.trim();
            	if (responseStr === "SAVE"){
            		alert("SAVE");
            	}else if ( responseStr === "LOGIN FAIL" ){
            		alert("로그인 후 이용해 주세요");
            	}else{
            		alert("FAIL");
            	}
            }
        });
    };

    this.reload = function (articleId) {
        $$.ajax ({
            method : 'GET',
            url : '../api/v1/highlight?article_id='+articleId,
            async : true,
            success : function (data) {
                hl.deserialize(JSON.parse(data));
            }
        });
    };

    this.reset = function (ev) {
        ev.preventDefault();

        var highlights = document.querySelectorAll("span[style^='background-color: rgb']");

        for (var i = 0, length = highlights.length; i < length ; i++) {
            highlights[i].style.backgroundColor = 'transparent';
        }
    }

    this.serialize = function () {
        var oData = {};
        var aSerialized = [];
        article.innerHTML = article.innerHTML.replace(/[\f\n\r\t\v]/g,"");
        var highlights = document.querySelectorAll("span[style^='background-color: rgb']")
        for (var i = 0, length = highlights.length; i < length; i++) {
            var oEle = {};
            var ele = highlights[i];
            var temp = hl.getTempSpan();
            ele.parentNode.insertBefore(temp, ele);

            oEle.color = ele.style.backgroundColor;
            oEle.start = article.textContent.indexOf('@#$@#$!!');
            oEle.text = [];

            for (var k = 0; k < ele.childNodes.length; k++) {
                var textContent = ele.childNodes[k].textContent;
                var aTextContent = textContent.split(/[\f\n\r\t\v]/g);
                for (var j = 0; j < aTextContent.length; j++) {
                    if(aTextContent[j].length >= 1) {
                        oEle.text.push(aTextContent[j]);
                    } else {
                        if (k == 0) {
                            oEle.start++;
                        };
                    }
                };

            };

            aSerialized.push(oEle);
            // Remove temp span.
            document.querySelector('.fingqooq-hidden').remove();
        };

        oData.article_id = location.pathname.split('/')[location.pathname.split('/').length-1];
        oData.data = aSerialized;

        return JSON.stringify(oData);
    };

    this.deserialize = function (aSerialized) {
        article.innerHTML = article.innerHTML.replace(/[\f\n\r\t\v]/g,"");
        for (var i = 0; i < aSerialized.length; i++) {
            var oEle = aSerialized[i];
            var pos = 0, count = 0;

            while(oEle.start != article.textContent.indexOf(oEle.text[0], pos)) {
                pos = article.textContent.indexOf(oEle.text[0], pos)+1;
                count++;
            };

            pos = 0;
            for (var j = 0; j < count; j++) {
                pos = article.innerHTML.indexOf(oEle.text[0], pos);
                pos++;
            };

            for (var k = 0; k < oEle.text.length; k++) {
                var text = oEle.text[k];
                article.innerHTML = article.innerHTML.substring(0, pos)
                                    + article.innerHTML.substring(pos).replace(text, "<span style='background-color: " + oEle.color + "'>" + text + "</span>");
            };
        };
    };

    this.getTempSpan = function () {
        var tempspan = document.createElement('span');
        tempspan.textContent = "@#$@#$!!";
        $$.addClass(tempspan, 'fingqooq-hidden');

        return tempspan;
    };
}




function makeEditableAndHighlight (colour) {
    var range, sel = window.getSelection();
    if (sel.rangeCount && sel.getRangeAt) {
        range = sel.getRangeAt(0);
    }
    document.designMode = "on";
    if (range) {
        sel.removeAllRanges();
        sel.addRange(range);
    }
    // Use HiliteColor since some browsers apply BackColor to the whole block
    if (!document.execCommand("HiliteColor", false, colour)) {
        document.execCommand("BackColor", false, colour);
    };

    sel.removeAllRanges();
    document.designMode = "off";
};

function highlightSelection (colour) {
    var range, sel;
    if (window.getSelection) {
        // IE9 and non-IE
        try {
            if (!document.execCommand("BackColor", false, colour)) {
                makeEditableAndHiight(colour);
            }
        } catch (ex) {
            makeEditableAndHighlight(colour)
        }
    } else if (document.selection && document.selection.createRange) {
        // IE <= 8 case
        range = document.selection.createRange();
        range.execCommand("BackColor", false, colour);
    }
};

// Element를 찾아서 DOM을 반환하는 함수.
// param : name
$$.getByName = function (param) {
    return document.getElementsByName(param)[0];
};

// Element를 찾아서 DOM을 반환하는 함수.
// param : id
$$.getById = function (param) {
    return document.getElementById(param);
};

// Info Object를 받아 ajax요청을 하는 함수, 내부에서 함수를 실행하거나 리턴함수로 넘겨줌
// oInfo = {
//  method : 요청 method(String),
//  url : 요청 url(String),
//  async : 비동기 - true, 동기 - false,
//  content : Content type,
//  sendData : 보낼 데이터, 기본값 null,
//  success : 성공시 실행되는 함수,
//  error : 에러시 실행되는 함수
// }
$$.ajax = function (oInfo) {
    var xhr, sendData;
    // code for IE7+, Firefox, Chrome, Opera, Safari
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    // code for IE6, IE5
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    };

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var responseText = xhr.responseText;
            oInfo.success(responseText);
        } else {
            if (oInfo.error) {
                oInfo.error();
            };
        }
    };

    xhr.open(oInfo.method, oInfo.url, oInfo.async);
    xhr.setRequestHeader("Content-Type", oInfo.content);

    sendData = oInfo.sendData || null;
    xhr.send(sendData);
};

// Dom의 class를 toggle하는 함수
$$.toggleClass = function (dom, className) {
    if (this.hasClass(dom, className)) {
        dom.classList.remove(className);
    } else {
        dom.classList.add(className);
    };
};

// Dom의 class가 없으면 추가하는 함수
$$.addClass = function (dom, className) {
    dom.classList.add(className);
};

// Dom의 class가 있으면 빼는 함수
$$.removeClass = function (dom, className) {
    dom.classList.remove(className);
};

// Dom이 className이 있으면 true, 없으면 false를 반환하는 함수
$$.hasClass = function (dom, className) {
    for (var i = 0; i < dom.classList.length; i++) {
        if (dom.classList[i] == className) {
            return true;
        };
    };

    return false;
};

var hl = new highlighter('#F6F94A');
hl.reload(location.pathname.split('/')[location.pathname.split('/').length-1]);
