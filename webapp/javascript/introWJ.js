// Gradation
// Made by WooGenius
// intro.html에 연결되는 자바스크립트 파일

// Intro 페이지에서 쓰는 Object
//namespace

var oIntro = {
	// URL DATA
	URL_DATA : {
		loginUrl : "intro/login",
		signupUrl : "intro/signup",
		signupDivUrl : "intro/signupDiv.html",
		loginDivUrl : "intro/loginDiv.html"
	},

	// init 함수
	init : function () {
		// Click 이벤트 등록.
		document.getElementById('login-btn').addEventListener('click', function () {
			oIntro.requestLogin();
		}, false);

		document.getElementById('signup-btn').addEventListener('click', function () {
			oIntro.getSignupDiv();
		}, false);
	},

	// 내부에서만 쓰는 함수들
	$ : {}
};

oIntro.init();

// Singup버튼을 signup div를 ajax요청하여, 뿌려주는 함수.
oIntro.getSignupDiv = function () {
	var that = this;
	this.$.getDiv('form-div', this.URL_DATA.signupDivUrl, function () {
		var signupBtn = that.$.getById('signup-btn');
		var goToLoginBtn = that.$.getById('goToLogin-btn');
		that.$.addClickListener(signupBtn, function () {
			that.requestSignup();
		});
		that.$.addClickListener(goToLoginBtn, function () {
			that.getLoginDiv();
		});
	});
};

// toLoginPage버튼을 클릭하면 loginDiv를 ajax요청하여, 뿌려주는 함수.
oIntro.getLoginDiv = function () {
	var that = this;
	this.$.getDiv('form-div', this.URL_DATA.loginDivUrl, function () {
		var signupBtn = that.$.getById('signup-btn');
		var loginBtn = that.$.getById('login-btn');
		that.$.addClickListener(signupBtn, function () {
			that.getSignupDiv();
		});
		that.$.addClickListener(loginBtn, function () {
			that.requestLogin();
		});
	});
};

// 로그인 요청이 유효한지 판단하고 유효하면, 폼을 submit하고 유효하지 않으면, Login Fail메세지를 띄우는 함수.
oIntro.requestLogin = function () {
	var that = this;
	var sendData = this.$.getLoginData();

	this.$.ajax ({
		method : 'POST',
		url : this.URL_DATA.loginUrl,
		async : true,
		content : 'application/json',
		sendData : JSON.stringify(sendData),
		success : function (data) {
			var jsonObj = JSON.parse(data);

			// 이메일 인증이 안되어있거나, 이메일 인증이 되어있을때 기본 url로 보냄
			if (jsonObj.code == 200 || jsonObj.code == 202) {
				location.href = './';
			} else {
				var warning = that.$.getById('warning');
				that.$.removeClass(warning, 'hidden');
				that.$.changeSingupBtn();
			}
		}
	});
};

// 서버에 signup요청을 ajax로 보내는 함수.
oIntro.requestSignup = function () {
	var that = this;
	var sendData = this.$.getSignupData();

	this.$.ajax ({
		method : 'POST',
		url : this.URL_DATA.signupUrl,
		async : true,
		content : 'application/json',
		sendData : JSON.stringify(sendData),
		success : function (data) {
			var jsonObj = JSON.parse(data);

			// 이메일 인증이 안되어있거나, 이메일 인증이 되어있을때 기본 url로 보냄
			if (jsonObj.code === "200") {
				alert("Sign Up Success. Before login, You need to verify your Email.")
				location.href='';
			} else {
				var warning = that.$.getById('warning');
				that.$.removeClass(warning, 'hidden');
				warning.textContent = jsonObj.message;
			}
		}
	});
}

// Element를 찾아서 DOM을 반환하는 함수.
// param : name
oIntro.$.getByName = function (param) {
	return document.getElementsByName(param)[0];
};

// Element를 찾아서 DOM을 반환하는 함수.
// param : id
oIntro.$.getById = function (param) {
	return document.getElementById(param);
};

// Info Object를 받아 ajax요청을 하는 함수, 내부에서 함수를 실행하거나 리턴함수로 넘겨줌
// oInfo = {
// 	method : 요청 method(String),
// 	url : 요청 url(String),
// 	async : 비동기 - true, 동기 - false,
// 	content : Content type,
//  sendData : 보낼 데이터, 기본값 null,
// 	success : 성공시 실행되는 함수,
// 	error : 에러시 실행되는 함수
// }
oIntro.$.ajax = function (oInfo) {
	var xhr, sendData;
	// code for IE7+, Firefox, Chrome, Opera, Safari
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	// code for IE6, IE5
	} else {
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	};

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4 && xhr.status === 200) {
			var responseText = xhr.responseText;
			oInfo.success(responseText);
		} else {
			if (oInfo.error) {
				oInfo.error();
			};
		}
	};

	xhr.open(oInfo.method, oInfo.url, oInfo.async);
	xhr.setRequestHeader("Content-Type", oInfo.content);

	sendData = oInfo.sendData || null;
	xhr.send(sendData);
};

// Dom의 class를 toggle하는 함수
oIntro.$.toggleClass = function (dom, className) {
	if (this.hasClass(dom, className)) {
		dom.classList.remove(className);
	} else {
		dom.classList.add(className);
	};
};

// Dom의 class가 없으면 추가하는 함수
oIntro.$.addClass = function (dom, className) {
	dom.classList.add(className);
};

// Dom의 class가 있으면 빼는 함수
oIntro.$.removeClass = function (dom, className) {
	dom.classList.remove(className);
};

// Dom이 className이 있으면 true, 없으면 false를 반환하는 함수
oIntro.$.hasClass = function (dom, className) {
	for (var i = 0; i < dom.classList.length; i++) {
		if (dom.classList[i] == className) {
			return true;
		};
	};

	return false;
};

// Signup form에서 data를 object형태로 가져오는 함수
oIntro.$.getSignupData = function () {
	var data = {};
	var email = this.getByName('email').value;
	var password = this.getByName('password').value;
	var passwordConfirm = this.getByName('password-confirm').value;
	var name = this.getByName('name').value;
	var birthDay = this.getByName('birth-year').value + '-' +
					this.getByName('birth-month').value + '-' +
					this.getByName('birth-day').value;
	var gender = this.getRadioBtnChecked('gender');

	data['email'] = email;
	data['password'] = password;
	data['password-confirm'] = passwordConfirm;
	data['name'] = name;
	data['birthDay'] = birthDay;
	data['gender'] = gender;

	return data;
};

// check된 radio button의 value를 가져오는 함수
oIntro.$.getRadioBtnChecked = function (name) {
	var radio = document.getElementsByName(name);
	for (var i = 0; i < radio.length; i++) {
		if (radio[i].checked) {
			return radio[i].value;
		};
	};
};

// Login form에서 data를 object형태로 가져오는 함수
oIntro.$.getLoginData = function () {
	var data = {};
	var email = this.getByName('email').value;
	var password = this.getByName('password').value;

	data.email = email;
	data.password = password;

	return data;
};

// Email을 적엇을때 Singup button의 내용을 바꾸는 함수
oIntro.$.changeSingupBtn = function () {
	var email = this.getByName('email').value;
	var signupBtn = this.getById('signup-btn');
	if (email) {
		signupBtn.textContent = "Sign Up With This E-Mail";
	};
};

// url로부터 div를 가져와서 바꾸고자 하는 div안의 내용을 바꾸는 함수
// divId : 내용을 바꾸고자 하는 div의 id
// divUrl : 가져오고자 하는 div의 url
// callback : 페이지를 가져온 후 실행할 함수
oIntro.$.getDiv = function (divId, divUrl, callback) {
	var that = this;
	this.ajax({
		method : 'GET',
		url : divUrl,
		async : true,
		content : 'application/x-www/form/urlencoded',
		success : function (data) {
			var email = that.getByName('email').value;
			var password = that.getByName('password').value;

			that.setDiv(divId, data);

			that.getByName('email').value = email;
			that.getByName('password').value = password;

			callback();
		}
	});
};

// div안의 내용을 data로 바꾸는 함수
oIntro.$.setDiv = function (divId, data) {
	var div = this.getById(divId);
	div.innerHTML = '';
	div.innerHTML = data;
};

// click 이벤트를 추가하는 함수
oIntro.$.addClickListener = function (dom , callback) {
	dom.addEventListener('click', callback, false);
};