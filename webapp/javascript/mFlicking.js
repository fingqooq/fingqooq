window.mFlicking = (function() {
	
	var nContainerWidth = 0;
	// position of "touchsatrt" event 
	var nTouchStartX = 0;
	var nTouchStartY = 0;
	
	// position of "touchmove, touchend" event
	var nTouchX = 0;
	var nTouchY = 0;
	
	var nTimeout = 0;
	
	mFlicking.prototype._attachTouchStart = function() {
		var _htElement = this._htElement;
		EventUtil.addHandler(_htElement["elFlick"], "touchstart", function(e) {
			this._clearAnchor();
			var touch = e.touches[0];
			// position
			nTouchStartX = touch.pageX;
			nTouchStartY = touch.pageY;
		}.bind(this));
	};
	
	mFlicking.prototype._setElement = function() {
		var _htElement = this._htElement;
		var _htOption = this._htOption;
		
		_htElement["elFlick"] = $$$("." + _htOption.view);
		_htElement["elContainer"] = $$$("." + _htOption.container);
		_htElement["aChildNodes"] = $$$("." + _htOption.childnode, _htElement["elContainer"]);
		this.nNumOfPanel = _htElement["aChildNodes"].length;
		nContainerWidth = _htElement["elContainer"].offsetWidth;
	};
	
	mFlicking.prototype._attachTouchMove = function() {
		var _htElement = this._htElement;
		EventUtil.addHandler(_htElement["elFlick"], "touchmove", function(e) {
			EventUtil.preventDefault(e);
			var touch = e.touches[0];
			// position
			nTouchX = touch.pageX;
			nTouchY = touch.pageY;
			
			var nValue = nTouchX - nTouchStartX;
			if (this.nIndex <= 0 && nValue > 0 || this.nIndex >= this.nNumOfPanel - 1 && nValue < 0) {
				return false;
			} else {
				_htElement["elContainer"].style.webkitTransform = "translate(" + nValue + "px)";
			}
		}.bind(this));
	};
	
	mFlicking.prototype._attachTouchEnd = function() {
		var _htElement = this._htElement;
		EventUtil.addHandler(_htElement["elFlick"], "touchend", function(e) {
			var touch = e.changedTouches[0];
			// position
			nTouchX = touch.pageX;
			nTouchY = touch.pageY;
			
			var nTranslate = nContainerWidth;
			var nTmpIndex = this.nIndex;
			
			if (Math.abs(nTouchStartX - nTouchX) <= 100) {
				this._restoreAnchor();
				this._setAnchorElement();
				_htElement["elContainer"].style.webkitTransition = "all 0.1s linear";
				_htElement["elContainer"].style.webkitTransform = "translate(0)";
				return false;
			}
			
			if (nTouchStartX - nTouchX > 0) {
				this.nIndex++;
				nTranslate = nContainerWidth * -1;
			} else {
				this.nIndex--;
			}
			if (this.nIndex >= 0 && this.nIndex <= this.nNumOfPanel - 1) {
				nTimeout = setTimeout(function() {
					this._setPosition();
					_htElement["elContainer"].style.webkitTransform = "translate(0)";
					_htElement["elContainer"].style.webkitTransition = null;
				}.bind(this), 200);
				
				_htElement["elContainer"].style.webkitTransition = "all 0.2s ease-out";
				_htElement["elContainer"].style.webkitTransform = "translate(" + nTranslate + "px)";
			} else {
				this.nIndex = nTmpIndex;
			}
		}.bind(this));
	};
	
	mFlicking.prototype._setAnchorElement = function() {
		var _htElement = this._htElement;
		if (window.detector.os() === "iOS") {
			this._aAnchor = $$$("A", _htElement["elContainer"]);
		}
	};
	
	mFlicking.prototype._clearAnchor = function() {
		if (this._bBlocked || !this._aAnchor) {
			return false;
		}
		
		this._fnDummyFnc = function() {
			return false;
		};
		for (var i = 0, nLen = this._aAnchor.length; i < nLen; i++) {
			if (this._fnDummyFnc !== this._aAnchor[i].onclick) {
				this._aAnchor[i]._onclick = this._aAnchor[i].onclick;
			}
			this._aAnchor[i].onclick = this._fnDummyFnc;
		}
		this._bBlocked = true;
	};
	
	mFlicking.prototype._restoreAnchor = function() {
		if (!this._bBlocked || !this._aAnchor) {
			return false;
		}
		
		for (var i = 0, nLen = this._aAnchor.length; i < nLen; i++) {
			if (this._fnDummyFnc !== this._aAnchor[i]._onclick) {
				this._aAnchor[i].onclick = this._aAnchor[i]._onclick;
			} else {
				this._aAnchor[i].onclick = null;
			}
		}
		this._bBlocked = false;
	};
	
	mFlicking.prototype._setPosition = function() {
		var _htElement = this._htElement;
		for (var i = 0; i < this.nNumOfPanel; i++) {
			_htElement["aChildNodes"][i].style.left = ((i - this.nIndex) * 100) + "%"; 
		}
	};
	
	function mFlicking(htOption) {
		this._htElement = [];
	
		this._htOption = htOption || {
			view : "flickview",
			container : "flick-container",
			childnode : "flick-panel"
		};
		
		// index of the element currently on-screen
		this.nIndex = 0;
		// number of panels
		this.nNumOfPanel = 0;
	}
	
	return mFlicking;
})();
