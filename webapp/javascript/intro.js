var oIntroByTs = (function() {
/* 이벤트의 대상이 되는 element 들의 이름을 모아놓는 객체 */
	var oEvElems = {
		'profile-pic' : 'profile',
		'desc-popup' : 'member-desc',
		'signup' : 'btn-large btn-signup',
		'member-intro-wrap' : 'member-intro'
	};
	
	EventUtil.addHandler(window, "load", function (e) {
		var elMemberIntroWrap = $$$("." + oEvElems['member-intro-wrap']);
		
		EventUtil.addHandler(elMemberIntroWrap, "mouseover", function(e) {
			var elTarget = e.target;
			var elPopup = $$$("." + oEvElems['desc-popup'], elTarget.parentElement);
			
			if (elTarget.className.search(oEvElems['profile-pic']) > -1 || elTarget.className.search(oEvElems['desc-popup']) > -1) {
				setElemVisible(elPopup, true);
				setPopupPos(elPopup);
			}
		});
		EventUtil.addHandler(elMemberIntroWrap, "mouseout", function(e) {
			var elTarget = e.target;
			var elPopup = $$$("." + oEvElems['desc-popup'], elTarget.parentElement);
			
			if (elTarget.className.search(oEvElems['profile-pic']) > -1 || elTarget.className.search(oEvElems['desc-popup']) > -1) {
				setElemVisible(elPopup, false);
			}
		});	
	});
	
	function setElemVisible(elTarget, flag) {
		/* 
		 * flag :
		 * true = 'show'
		 * false = 'hide'
		 */
		if (flag) {
			elTarget.style.visibility = "visible";
		} else {
			elTarget.style.visibility = "hidden";
		}
	}
	
	function setPopupPos(elPopup) {
		
		var elProfilePic = $$$(".profile", elPopup.parentElement);
		
		/* getBoundingClientRect() 함수는 현재 뷰포트에서의 위치 값을 가진 object 를 반환함
		 */
		var nProfileViewportTop = elProfilePic.getBoundingClientRect().top;
		
		/* Viewport 의 크기를 구하려면 document.documentElement 을 활용한다.
		 * 브라우저별로 window 등을 활용할 수도 있음. */
		var nViewportHeight = document.documentElement.clientHeight;
		
		/* element가 parent element와의 비교에서 위치값을 어떻게 가지고 있는지를 알려면 element.offsetTop
		 * 을 사용하면 된다.
		 */
		var nProfileOffsetTop = elProfilePic.offsetTop;
		var nProfileOffsetLeft = elProfilePic.offsetLeft;
		
		/* element 의 현재 너비와 높이 값을 얻는데는 clientHeight, scrollHeight, offsetHeight 등이
		 * 사용된다
		 */
		var nMargin = 3;
		var nPopupHeight = elPopup.clientHeight;
		var nOffsetTop = elProfilePic.clientHeight + nMargin;
		
		/* 이런 식으로 element 의 위치를 조절하려면 반드시 해당 element의 position 속성이 absolute
		 * 로 설정되어 있어야 함 
		 */
		if (nViewportHeight < nProfileViewportTop + nOffsetTop + nPopupHeight) {
			/* 만약 elPopup의 맨 아랫부분이 viewport의 크기보다 밑에 있어서 짤리게 될 경우에는
			 * elPopup를 프로필 사진의 위쪽에 붙인다.
			 */
			nOffsetTop = (-1 * nPopupHeight ) - nMargin;
		}
			elPopup.style.top = nProfileOffsetTop + nOffsetTop + "px";
			elPopup.style.left = nProfileOffsetLeft + "px";
		
	}
	
	/*
	 * JSON 으로 만들 데이터가 들어있는 Form 데이터들의 name 속성 배열을 getElemsValue 함수의
	 * 인자로 넘기면 각 form데이터들의 value값을 Object로 만들어 리턴한다
	 */ 
	function getElemsValueByName(aElemName) {
		var oElemVal = {};
		var sKey = "";
		
		for (var i = 0; i < aElemName.length; i++) {
			sKey = aElemName[i];
			/*
			 * aElemName 배열에는 element들의 name 속성이 들어있으므로
			 * $$$(querySelctor) 에서 name 속성의 값으로 선택하는 selector를 만든다.
			 */
			oElemVal[sKey] = $$$("[name='" + sKey + "']").value;
		}
		
		return oElemVal;
	}
})();