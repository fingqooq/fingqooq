(function($) {
	/**
	 * OS Hash Table
	 */
	
	console.log($);
	
	var htOs = {
		iOS : /like Mac OS X/,
		Android : /Android/
	};
	
	/**
	 * Device Hash Table
	 */
	var htDevice = {
		iphone : /iPhone/,
		ipad : /iPad/,
		galaxyS : /SHW-M110/,
		galaxyS2 : /SHW-M250|GT-I9100|SHV-E110/,
		galaxyS3 : /SHV-E210|SHW-M440|GT-I9300/,
		galaxyS4 : /SHV-E300|GT-i9500|SGH-M919|SPH-L720|SGH-I337|SCH-I545/,
		optimusLte : /LG-LU6200/,
		optimusLte2 : /LG-F160/,
		optimusG2 : /LG VS980/
	};
	
	/**
	 * go through hash table and if matches, return true
	 */
	function eachHash(ht) {
		for (var key in ht) {
			if (ht[key].test(navigator.userAgent)) {
				return key;
			}
		}
		
		return "";
	};
	
	$.detector = {
		/**
		 * return os name
		 */
		os : function() {
			return eachHash(htOs);
		},
		
		/**
		 * return os version
		 */
		osVersion : function() {
			var version = "", a;
			switch (this.os()) {
				case "iOS":
					a = navigator.userAgent.match(/OS\s([\d|\_]+\s)/i);
					break;
				case "Android":
					a = navigator.userAgent.match(/Android\s([^\;]*)/i);
					break;
			}
			if (a != null && a.length > 1) {
				version = a[1].replace(/\_/g, ".").replace(/\s/g, "");
			}
			return version;
		},
		
		/**
		 * return device
		 */
		device : function() {
			return eachHash(htDevice);
		}
	};
})(window);
