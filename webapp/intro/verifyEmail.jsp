<%--
  Created by IntelliJ IDEA.
  User: WooGenius
  Date: 4/21/14
  Time: 1:13 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Gradation</title>
        <meta content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link href="../css/reset.css" type="text/css" rel="stylesheet" />
        <link href="../css/intro.css" type="text/css" rel="stylesheet" />
        <link href="../css/divCtrl.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <div class="fullPage">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="large">Gradation</div>
                        <div class="small">
                            Small Differences <br class="sm"/>and Changes in Us.
                        </div>
                    </div>
                    <div class="col-md-6 center">
                        <div class="medium">
                            <%
                                Object result = request.getAttribute("result");
                                if (result.toString().equals("1")) {
                                    out.println("Email Verification Successed!");
                                } else {
                                    out.println("Email Verification Failed!");
                                }
                            %>
                        </div>
                        <button type="button" class="button" onclick="window.location.href ='../intro'">Go to LogIn Page</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>