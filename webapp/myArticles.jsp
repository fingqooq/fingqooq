<%@ page import="java.util.ArrayList"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>${profile.name}</title>

    <meta id="viewport" name="viewport"
          content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/article.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-52420606-1', 'auto');
ga('send', 'pageview');

	</script>
</head>

<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand c-red" href="../articles">FINGQOOQ</a>
        </div>
        <div class="navbar-collapse collapse navbar-inverse-collapse">
			<ul class="nav navbar-nav">
                    <c:if test="${session}"> <li><a id="btnMyArticles" href = ${myarticles}>MY ARTICLES</a></li></c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right">
					<c:if test="${session}"><li><a id="btnLogout" href = "/LogoutUser">LOGOUT</a></li></c:if>
                	<c:if test="${session == false }"><li><a id="btnLogin" href = "/LoginWithFB">LOGIN with FB</a></li></c:if>
            </ul>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel-profile">
                 <div class="profile-body">
					<c:choose>
						<c:when test="${profile.name!='unknown_user'}">
							<img src=${profile.img} alt=${profile.name}>
						</c:when>
						<c:otherwise>
							<img src="../img/unknown_user.jpg" alt=${profile.name}>
						</c:otherwise>
					</c:choose>
                    
                    <div class="profile-header">
                        <h2>${profile.name}</h2>
                    </div>
                    <div class="profile-footer">
                        <ul>
                            <li><span class="number myqooq-status">${profile.numOfQooq}</span><br>QOOQ</li>
                            <li><span class="number">${profile.numOfRead}</span><br> READ</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <c:forEach items="${articles}" var="article">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="../article/<c:out value="${article.id}"/>">
                            <div class="article-title">
                                    ${article.title}
                            </div>
                        </a>
                    </div>
                    <div class="panel-footer ta-right">
                    <span class="qooq-info"> 
                                		READ <span class="c-red">${article.read}</span> · QOOQ 
                                		<span class="c-red qooq-count">${article.qooq}</span>
                                		</span>
                        <a data-aid="<c:out value="${article.id}"/>" class="btn btn-default qooq-btn unqooq-status btn-sm">Q</a>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>



<div id="fb-root"></div>
<script src="../javascript/jquery-1.11.1.min.js"></script>
<script src="../javascript/bootstrap.min.js"></script>
<script>
function qooqArticle (aid, el) {
	var elQooqBtn = el;
    var bQooq = !$(elQooqBtn).hasClass('unqooq-status');
    var oAjaxParam = {
        callback 	: function() {
            var responseStr = xhr.responseText;
            var jsonObj = JSON.parse(responseStr)
            var nMyQooqCount = jsonObj.myQooQ;
            var elQooqBtn = el;
            if (jsonObj.code == 200) {
            	var parentNode = elQooqBtn.parentNode.parentNode.parentNode;
            	parentNode.classList.add('fadeout');
            	parentNode.classList.add('opacity-0');
            	setTimeout(function() {
            		parentNode.style.display = "none";
            	},400);
            	document.querySelector('.myqooq-status').innerHTML = nMyQooqCount; 
            } else {
                alret("Try Again!");
            }
        },
        method 		: 'GET',
        url			: '/qooq?id=' + aid + '&qooq=' + bQooq,
        async		: false,
        contentType : 'application/json',
        form 		: null
    };

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            oAjaxParam.callback(xhr);
        }
    };

    xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
    xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
    xhr.send(oAjaxParam.form);
}

document.addEventListener("click", function(e) {
	if (e.target.classList.contains('qooq-btn')) {
        e.preventDefault();
		var elTarget = e.target;
		var nArticleId = elTarget.getAttribute('data-aid');
		qooqArticle(nArticleId, elTarget);
	}
});
</script>

</body>
</html>
