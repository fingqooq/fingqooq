<%@ page import="java.util.ArrayList"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>FINGQOOQ</title>

<meta id="viewport" name="viewport"
	content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/article.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/recommend.css">
    
    <!-- google analytics -->
    <script>	
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-52420606-1', 'auto');
ga('send', 'pageview');
	</script>
	
	<!-- UserVoice JavaScript SDK (only needed once on a page) -->
	<script>
(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/KzpeJGhc9Jz62M0vcUGhA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()
	</script>

	<!-- A tab to launch the Classic Widget -->
	<script>
UserVoice = window.UserVoice || [];
UserVoice.push(['showTab', 'classic_widget', {
  mode: 'full',
  primary_color: '#ffffff',
  link_color: '#d9230f',
  default_mode: 'feedback',
  forum_id: 257582,
  tab_label: 'Feedback',
  tab_color: '#d9230f',
  tab_position: 'middle-right',
  tab_inverted: false
}]);
	</script>
</head>

<body>
    <div class="navbar navbar-default navbar-fixed-top">
    	<div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand c-red" href="./articles">FINGQOOQ</a>
            </div>
            <div class="navbar-collapse collapse navbar-inverse-collapse">
                <ul class="nav navbar-nav">
                    <c:if test="${session}"> <li><a id="btnMyArticles" href = ${myarticles}>MY ARTICLES</a></li></c:if>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                	<c:if test="${session}"><li><a id="btnLogout" href = "/LogoutUser">LOGOUT</a></li></c:if>
                	<c:if test="${session == false }"><li><a id="btnLogin" href = "/LoginWithFB">LOGIN with FB</a></li></c:if>
                </ul>
            </div>
        </div>
    </div>
        <div class="container">
			<div class="row recommend-row">
	            <div class="col-sm-12">
                    <div class="size-wrap">
		            	<div class="flickingWrapper">
							<div class="flicking">
		                        <p id ="recommend">Recommend<p>
								<ul>
						            <c:forEach items="${recommends}" var="recommend">
						            	<li>
							                <div class="pageCnt n1">
							                    <div class="recommend-contents panel panel-default">
							                        <div class="recommend-body panel-body">
							                            <a href="./article/<c:out value="${recommend.id}"/>">
							                                <div class="article-title">${recommend.title}</div>
							                            </a>
							                        </div>
							                        <div class="recommend-footer panel-footer ta-right">
							                        <span class="qooq-info"> 
							                                		READ <span class="c-red read-count">${recommend.read}</span> · QOOQ 
							                                		<span class="c-red qooq-count">${recommend.qooq}</span>
							                                		</span>
							                            <c:choose>
							                        	<c:when test="${session}">
							                        		<c:choose>
							                        			<c:when test="${recommend.didQooq == 0}">
							                            			<a data-aid="<c:out value="${recommend.id}"/>" class="btn btn-primary qooq-btn btn-sm qooqable">Q</a>
							                            		</c:when>
							                            		<c:otherwise>
							                            			<a data-aid="<c:out value="${recommend.id}"/>" class="btn btn-primary qooq-btn unqooq-status btn-sm qooqable">Q</a>
							                            		</c:otherwise>
							                            	</c:choose>
							                            </c:when>
							                            <c:otherwise>
							                            	<a onclick="alert('Login후 이용하세요.')" class="btn btn-primary qooq-btn btn-sm">Q</a>
							                            </c:otherwise>
							                        	</c:choose>
							                        </div>
							                    </div>
							                </div>
						                </li>
						            </c:forEach>
								</ul>
							</div>
		                    <a href="#" class="prev"><img src="img/btn_flicking_prev.png" alt="이전"></a>
		                    <a href="#" class="next"><img src="img/btn_flicking_next.png" alt="다음"></a>
							<div class="flickPaging">
								<a href="#" class="">&#149;</a>
								<a href="#" class="">&#149;</a>
								<a href="#" class="">&#149;</a>
								<a href="#" class="">&#149;</a>
								<a href="#" class="on">&#149;</a>
							</div> 
						</div>
					</div>
				</div>
			</div>
            
            <div class="row main-articles">
            <c:forEach items="${articles}" var="article">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <a href="./article/<c:out value="${article.id}"/>">
                                <div class="article-title">${article.title}</div>
                            </a>
                        </div>
                        <div class="panel-footer ta-right">
                        <span class="qooq-info"> 
                                		READ <span class="c-red read-count">${article.read}</span> · QOOQ 
                                		<span class="c-red qooq-count">${article.qooq}</span>
                                		</span>
                            <c:choose>
                        	<c:when test="${session}">
                        		<c:choose>
                        			<c:when test="${article.didQooq == 0}">
                            			<a data-aid="<c:out value="${article.id}"/>" class="btn btn-primary qooq-btn btn-sm qooqable">Q</a>
                            		</c:when>
                            		<c:otherwise>
                            			<a data-aid="<c:out value="${article.id}"/>" class="btn btn-primary qooq-btn unqooq-status btn-sm qooqable">Q</a>
                            		</c:otherwise>
                            	</c:choose>
                            </c:when>
                            <c:otherwise>
                            	<a onclick="alert('Login후 이용하세요.')" class="btn btn-primary qooq-btn btn-sm">Q</a>
                            </c:otherwise>
                        	</c:choose>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>


		<input type="hidden" id="scroll-check" value="0" />		
    <div id="fb-root"></div>
    <script src="../javascript/jquery-1.11.1.min.js"></script>
    <script src="../javascript/bootstrap.min.js"></script>
    <script src="../javascript/articles.js"></script>
    <script src="../javascript/flicking.js"></script>
    <script src="../javascript/paging-in-main.js"></script>

    <script>
        function qooqArticle (aid, el) {
        	var elQooqBtn = el;
        	var bQooq = !$(elQooqBtn).hasClass('unqooq-status');
            var oAjaxParam = {
                callback 	: function() {
                    var responseStr = xhr.responseText;
                    var jsonObj = JSON.parse(responseStr);
                    if (jsonObj.code == 200) {
                        elQooqBtn.classList.toggle('unqooq-status');
                        if (elQooqBtn.classList.contains('unqooq-status')) {
                        	elQooqBtn.innerHTML = 'Q';
                        } else {
                        	elQooqBtn.innerHTML = 'Q';
                        }
                        var nQooqCount = jsonObj.QooQ;
                        var nMyQooqCount = jsonObj.myQooQ;
                        elQooqBtn.parentNode.querySelector('.qooq-count').innerHTML = nQooqCount;
//                        document.querySelector('.myqooq-status').innerHTML = nMyQooqCount;
                    } else {
                        alret("Try Again!");
                    }
                },
                method 		: 'GET',
                url			: '/qooq?id=' + aid + '&qooq=' + bQooq,
                async		: false,
                contentType : 'application/json',
                form 		: null
            };
            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    oAjaxParam.callback(xhr);
                }
            };

            xhr.open(oAjaxParam.method, oAjaxParam.url, oAjaxParam.async);
            xhr.setRequestHeader("Content-Type", oAjaxParam.contentType);
            xhr.send(oAjaxParam.form);
        }
        
        document.addEventListener("click", function(e) {
        	if (e.target.classList.contains('qooqable')) {
                e.preventDefault();
                var elTarget = e.target;
        		var nArticleId = elTarget.getAttribute('data-aid');
        		qooqArticle(nArticleId, elTarget);
        	}
        });
        
    </script>
    <script>
		$(window).load(function(){
			$(document).ready(function() {
				$(".flickingWrapper").touchSlider({
					viewport: ".flicking",				
					prev : ".prev",
					next : ".next",
					pagination : ".flickPaging > a",	
					currentClass : "on",				
					duration: 500						
				});

			});
		});
	</script>
</body>
</html>
