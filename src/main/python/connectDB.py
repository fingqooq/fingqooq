import MySQLdb
from DBconfig import *

class DB():
    def __init__(self):
        self.db = MySQLdb.connect(host = DATABASE_HOST,
                                  port = DATABASE_PORT,
                                  user = DATABASE_USER,
                                  passwd = DATABASE_PW,
                                  db = DATABASE_DATABASE)
        self.cursor = self.db.cursor()


    def insertParserHTML(self, title, url, contents):
        self.cursor.execute("insert into article (title, url, contents) values (%s, %s, %s)", (title, url, contents))
        self.db.commit()

    def insertQooq(self, user_id, dbUrl):
        arg = (user_id, dbUrl)
        self.cursor.callproc('INSERT_QOOQ_URL', arg)
        self.db.commit()

    def insertError(self, user_id, dbUrl):
        self.cursor.execute("insert into error_page (uid, url) values (%s, %s)", (user_id, dbUrl))
        self.db.commit()
