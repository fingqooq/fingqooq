
#-*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup, NavigableString, Tag
import urllib2
import urllib
import operator
import nltk
import numpy
import math
import sys
from connectDB import DB

#url을 soup으로 만드는 함수
def makeSoup(url):
    f = urllib2.urlopen(url)
    page = f.read()
    soup = BeautifulSoup(page)
    
    if soup.originalEncoding != 'utf-8':
        header =  urllib.urlopen(url).headers.getheader('Content-Type')
        if header.split("=")[1] == 'euc-kr':
            page = page.decode('euc-kr', "replace")
        else:
            page = page.decode('utf-8', "replace")
        
        soup = BeautifulSoup(page)
    f.close()
    return soup


#html 내 script제거
def deleteCSS(soup):
    [tag.decompose() for tag in soup("script")]
    [tag.decompose() for tag in soup("style")]
    body = soup.find('body')
    return body

def get_content_len(str_list):
    count = 0
    
    len_dict = {}
    for line in str_list:
        if type(line) == NavigableString and len(line) > 10:
            len_dict[count] = len(line)
        
        count +=1
    return len_dict

def compare_body_len(a,b,c,d):
    gap_a = len(b.findAll(text=True)) - len(a.findAll(text=True))
    gap_b = len(c.findAll(text=True)) - len(b.findAll(text=True))
    gap_c = len(d.findAll(text=True)) - len(c.findAll(text=True))
    len_a = len(a.findAll(text=True))
    len_b = len(b.findAll(text=True))
    
    # print "len__________"
    # print len(a.findAll(text=True))
    # print len(b.findAll(text=True))
    # print len(c.findAll(text=True))
    # print len(d.findAll(text=True))
    #
    # print "gap__________"
    # print gap_a
    # print gap_b
    # print gap_c
    # print "contents is__________"
    if float(len_b)/len_a < 1.3: #mac str의 아버지가 바로 본문
        return delete_a_tag(a)
    elif gap_a > gap_b and len(b) > 5 or gap_b > gap_a * 2:
        return b
    elif gap_b > gap_c or gap_c > (gap_b)*2:
        return check_children(b,c)
    else:
        return check_children(c,d)

def standardDeviation(values, option):
    if len(values) < 2:
        return None
    
    sd = 0.0
    sum = 0.0
    meanValue = numpy.mean(values)
    
    for i in range(0, len(values)):
        diff = values[i] - meanValue
        sum += diff * diff
    
    sd = math.sqrt(sum / (len(values) - option))
    return sd

def get_total(node):
    total = []
    
    children = node.findChildren()
    for child in children:
        for i in child.findAll(text=True):
            total.append(len(i))
    return total


def check_children(node1, node2):
    node1_val = standardDeviation(get_total(node1),0)
    node2_val = standardDeviation(get_total(node2),0)
    if node1_val > node2_val:
        return node1
    else:
        return node2

def find_contents(content):
    a = content.parent
    b = a.parent
    c = b.parent
    d = c.parent
    
    return compare_body_len(a,b,c,d)

def prettify_contents(contents):
    global dbContents
    dbContents = contents.prettify()

def get_real_content(str_list, len_dict):
    result = None
    parent_list = []
    
    sorted_dict = sorted(len_dict.iteritems(), key=operator.itemgetter(1), reverse=True)[:5]
    
    for i in sorted_dict:
        idx = i[0]
        parent_list.append(len(str_list[idx].parent.parent))

    freq = nltk.FreqDist(parent_list)
    
    idx = 0
    for i in parent_list:
        if i == freq.max():
            content_idx = sorted_dict[idx][0]
            result =  str_list[content_idx]
            break
        idx += 1
    return result

def delete_a_tag(contents):
    children = contents.findChildren()
    
    idx = 0
    for child in children:
        if not (child.name == 'br' or child.name == 'p'):
            if  child.name == 'a' or len(child) < 5:
                children[idx].extract()
        idx += 1
    return contents

def run(url):
    global dbTitle
    
    soup = makeSoup(url)
    title = soup.title.string
    dbTitle = str(title)
    body = deleteCSS(soup)
    str_list = body.findAll(text=True)
    
    len_dict = get_content_len(str_list)
    content = get_real_content(str_list, len_dict)
    contents = find_contents(content)
    prettify_contents(contents)


if __name__ == "__main__":
    try:
        dbUrl = sys.argv[1]
        run(dbUrl)
        #db = DB()
        #db.insertParserHTML(dbTitle, dbUrl, dbContents)
        print "SUCCESS"
    except:
        print "FAIL"