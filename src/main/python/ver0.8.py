#-*- coding: utf-8 -*-
__author__ = 'youngnam'



from BeautifulSoup import BeautifulSoup, NavigableString, Tag
import urllib2
import urllib
import operator
import nltk
import numpy
import math
import sys
from connectDB import DB

#url을 soup으로 만드는 함수
def makeSoup(url):
    f = urllib2.urlopen(url)
    page = f.read()
    soup = BeautifulSoup(page)

    if soup.originalEncoding != 'utf-8':
        try:
            header =  urllib.urlopen(url).headers.getheader('Content-Type')
            if header.split("=")[1] == 'euc-kr':
                page = page.decode('euc-kr', "replace")
            else:
                page = page.decode('utf-8', "replace")
            soup = BeautifulSoup(page)
        except:
            soup = BeautifulSoup(page)

    f.close()
    return soup


#html 내 script제거
def deleteCSS(soup):
    [tag.decompose() for tag in soup("script")]
    [tag.decompose() for tag in soup("style")]
    body = soup.find('body')
    return body

def get_content_len(str_list):
    count = 0

    len_dict = {}
    for line in str_list:
        if type(line) == NavigableString and len(line) > 30:
            len_dict[count] = line.parent
            len_dict[count] = line.parent.parent
        count +=1
    return len_dict



def prettify_contents(contents):
    global dbContents
    dbContents = contents.prettify()

def get_real_content(len_dict):
    freq = nltk.FreqDist(len_dict.values())

    print freq.values()
    first = freq.keys()[0]
    second = freq.keys()[1]
    second_val = freq.values()[1]

    if second_val < 2 or (len(first) > len(second)):
        return first
    else:
        print "_____chage_ freq key"
        return second

def check_child(content):
    children = content.findChildren()

    max = None
    max_len = 0

    for child in children:
        if len(child) >= max_len:
            max_len = len(child)
            max = child
        try:
            if child.name == 'a' and child.parent.parent.name in ['ul', 'li']:
                child.parent.parent.extract()
        except:
            print "er"
            pass

    if max_len >= len(content):
        return max
    else:
        return content

def run(url):
    global dbTitle
    global dbContents

    soup = makeSoup(url)
    title = soup.title.string
    dbTitle = str(title)
    body = deleteCSS(soup)
    str_list = body.findAll(text=True)

    len_dict = get_content_len(str_list)
    content = get_real_content(len_dict)
    result = check_child(content)

    file = open("/Users/youngnam/Desktop/a.html", "w")

    prettify_contents(result)
    for i in dbContents:
        file.write(i)
    file.close()

#한곳에 모두 존재
a = "http://news.donga.com/NewsStand/3/all/20140616/64304460/1"
b="http://www.businesspost.co.kr/news/articleView.html?idxno=476"
c= "http://techcrunch.com/2014/06/29/facebook-and-the-ethics-of-user-manipulation/"
d = "http://techcrunch.com/2014/06/27/google-hangouts-will-no-longer-require-a-plugin-for-chrome-users/?ncid=rss&cps=gravity"
e = "http://www.yonhapnews.co.kr/international/2014/06/16/0606000000AKR20140616037200007.HTML?template=2085"
f="http://www.insight.co.kr/content.php?Idx=3978&Code1=003"
k = "http://www.venturesquare.net/539867"
h = "http://sungmooncho.com/2014/06/29/quality-customers/"
g = "http://sports.news.naver.com/sports/index.nhn?category=worldfootball&ctg=news&mod=read&office_id=109&article_id=0002818227"
run(a)
