#-*- coding: utf-8 -*-
__author__ = 'youngnam'

from BeautifulSoup import BeautifulSoup, NavigableString, Tag
import urllib2
import urllib
import nltk
from nltk.probability import FreqDist
import sys
from connectDB import DB

#url을 soup으로 만드는 함수
def makeSoup(url):
    f = urllib2.urlopen(url)
    page = f.read()
    soup = BeautifulSoup(page)

    if soup.originalEncoding != 'utf-8':
        try:
            utf8_list = ['utf8', 'utf-8', 'UTF-8', 'UTF8']
            header =  urllib.urlopen(url).headers.getheader('Content-Type')
            if header.split("=")[1] in utf8_list:
                page = page.decode('utf-8', "replace")
            else:
                page = page.decode('euc-kr', "replace")

            soup = BeautifulSoup(page)
        except:
            soup = BeautifulSoup(page)

    f.close()
    return soup


#html 내 script제거
def deleteCSS(soup):
    [tag.decompose() for tag in soup("script")]
    [tag.decompose() for tag in soup("style")]
    body = soup.find('body')
    return body

def get_content_len(str_list):
    count = 0

    len_dict = {}
    for line in str_list:
        if type(line) == NavigableString and len(line) > 30:
            len_dict[len(line)] = line.parent
            len_dict[len(line)] = line.parent.parent
        count +=1
    return len_dict



def prettify_contents(contents):
    global dbContents
    dbContents = contents.prettify()

def get_real_content(len_dict):
    freq = FreqDist(len_dict.values())

    str_len = len_dict.keys()
    str_len.sort()
    max_val = str_len[len(str_len)-1]
    next_val = str_len[len(str_len)-2]
    if float(next_val)/max_val < 0.09:
        return len_dict[max_val]

    first = freq.keys()[0]
    second = freq.keys()[1]
    second_val = freq.values()[1]

    if second_val < 2 or (len(first) > len(second)):
        return check_child(first)
    else:
        print "_____chage_ freq key"
        return check_child(first)

def check_child(content):
    children = content.findChildren()

    max = None
    max_len = 0

    for child in children:
        if len(child) >= max_len:
            print "___change__child"
            max_len = len(child)
            max = child
        try:
            if child.name == 'a' and child.parent.parent.name in ['ul', 'li']:
                child.parent.parent.extract()
        except:
            print "er"
            pass

    if max_len >= len(content):
        return max
    else:
        return content

def run(url):
    global dbTitle
    global dbContents

    soup = makeSoup(url)
    title = soup.title.string
    dbTitle = str(title)

    body = deleteCSS(soup)
    str_list = body.findAll(text=True)

    len_dict = get_content_len(str_list)
    result = get_real_content(len_dict)
    prettify_contents(result)

# run("http://www.vingle.net/posts/302190-%EA%B8%80%EC%9D%84-%EC%9E%98-%EC%93%B0%EB%8A%94-%EB%B0%A9%EB%B2%95?shsrc=fb")

if __name__ == "__main__":
    dbUrl = sys.argv[1]
    user = int(sys.argv[2])
    db = DB()
    try:
        run(dbUrl)
        db.insertParserHTML(dbTitle, dbUrl, dbContents)
        db.insertQooq(user, dbUrl)
    except:
        db.insertError(user, dbUrl)
