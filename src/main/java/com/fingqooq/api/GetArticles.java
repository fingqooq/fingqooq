package com.fingqooq.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.article.AllArticleModel;
import com.google.gson.Gson;

public class GetArticles extends HttpServlet {
       
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		
		ServletContext sc = getServletContext();
        AllArticleModel articlesDB = new AllArticleModel(sc);
		
		String responseString = "";
		
		
        HttpSession session = req.getSession(false);
        int user_id = 0;
        
        if (session != null && session.getAttribute("fb_id") != null)
        	user_id = (int)session.getAttribute("user_id");

        List articles = articlesDB.getAllArticlesList(user_id);
		
		
		Gson gson = new Gson();
		responseString = gson.toJson(articles);
		
		PrintWriter out = response.getWriter();
		
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		
		out.write(responseString);
	}
}
