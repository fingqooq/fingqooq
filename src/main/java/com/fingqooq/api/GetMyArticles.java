package com.fingqooq.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fingqooq.model.article.AllArticleModel;
import com.fingqooq.model.article.MyArticlesModel;
import com.fingqooq.model.user.UserInfo;
import com.fingqooq.model.user.UserInfoModel;
import com.google.gson.Gson;


public class GetMyArticles extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		
    	String path = request.getPathInfo();
        String fb_id = path.substring(1);
        
        ServletContext sc = getServletContext();
        
        UserInfoModel user = new UserInfoModel(sc, fb_id);
        UserInfo userInfo = user.getUserInfo();
        int userID = userInfo.getUserID();
		
		MyArticlesModel db = new MyArticlesModel(sc, userID);
		
		String responseString = "";

		List articles = db.getMyArticles();
		
		Gson gson = new Gson();
		responseString = gson.toJson(articles);
		
		PrintWriter out = response.getWriter();
		
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		
		out.write(responseString);
	}


}
