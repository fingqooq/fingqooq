package com.fingqooq.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.article.AllArticleModel;
import com.fingqooq.model.article.OneArticleModel;
import com.google.gson.Gson;

public class GetOneArticle extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		
		String requestUri = request.getRequestURI();
		int idxOfParam = requestUri.indexOf("articles") + 8;
		String parameterUri = requestUri.substring(idxOfParam);
		int articleId = Integer.parseInt(parameterUri.split("/")[1]);
		
		HttpSession session = request.getSession(false);
		Integer userId = null;
		
		if (session != null) {
			userId = (Integer)session.getAttribute("fb_id");
		}
		
		ServletContext sc = getServletContext();
        OneArticleModel db = new OneArticleModel(sc, articleId, userId);
		
		Map article = db.getArticle();
		String responseString = "";
		Gson gson = new Gson();
		responseString = gson.toJson(article);
		
		PrintWriter out = response.getWriter();

		
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		
		out.write(responseString);
	}
}
