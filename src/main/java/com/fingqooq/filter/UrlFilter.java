package com.fingqooq.filter;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UrlFilter implements Filter {
	private static final String DEFAULT_URL = "http://fingqooq.com";
	private static final String DEFAULT_INDEX = "fingqooq.com";
	private static final String LOCALHOST = "localhost:8080";
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse reps = (HttpServletResponse) response;
		
		String url = req.getRequestURL().toString();
		String indexPage = url.split("/")[2];
		
		if (indexPage.equals(DEFAULT_INDEX) || indexPage.equals(LOCALHOST)){
			chain.doFilter(request, response);
		}else{
			String path = req.getRequestURI();
			reps.sendRedirect(DEFAULT_URL+path);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
