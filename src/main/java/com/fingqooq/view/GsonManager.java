package com.fingqooq.view;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class GsonManager {
	private GsonBuilder gsonBuilder;
	private Type type;
	
	public GsonManager(TypeToken typeToken) {
		gsonBuilder = new GsonBuilder();
		type = typeToken.getType();
	}
	
	public Object jsonToObject(Reader in) {
		JsonReader jr = new JsonReader(in);
		Gson gson = gsonBuilder.create();
		Object result = gson.fromJson(jr, type);
		try {
			jr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Object jsonToObject(String str, Type type) {
		Gson gson = gsonBuilder.create();
		Object result = gson.fromJson(str, type);
		return result;
	}
	
	public String objectToJson(Object object) {
		Gson gson = gsonBuilder.create();
		return gson.toJson(object);
	}
	
	public void serializeNull() {
		gsonBuilder.serializeNulls();
	}
	
	public void skipThisField(String... fieldNames) {
		final List<String> params = new ArrayList<String>();
		for(String param : fieldNames) {
			params.add(param);
		}
		gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
			
			@Override
			public boolean shouldSkipField(FieldAttributes fieldAttr) {
				if (params.contains(fieldAttr.getName())) return true;
				else 									  return false;
			}
			
			@Override
			public boolean shouldSkipClass(Class<?> arg0) {
				// TODO Auto-generated method stub
				return false;
			}
		});
	}
	
	public void setType(TypeToken typeToken) {
		this.type = typeToken.getType();
	}
}
