package com.fingqooq.controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.user.UserInfo;
import com.fingqooq.model.user.UserInfoModel;
import com.google.gson.Gson;

public class GetUserInfo extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession(false);
		int user_id = (int)session.getAttribute("user_id");
		
		UserInfoModel uim = new UserInfoModel(getServletContext(), user_id);
		UserInfo userInfo = uim.getUserInfo();
		
		Gson gson = new Gson();
		String jsonStr = gson.toJson(userInfo);
		
		response.getWriter().println(jsonStr);
	}
}
