package com.fingqooq.controller.read;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.read.ReadArticleModel;
import com.google.gson.Gson;

public class UnreadArticle extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int articleId = Integer.parseInt(request.getParameter("article-id"));
		HttpSession session = request.getSession(false);
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		try {
			int userId = (int)session.getAttribute("user_id");
			
			ReadArticleModel ram = new ReadArticleModel(getServletContext());
			ram.deleteRead(articleId, userId);
			
			int readCount = ram.getRead(userId);
			
			
			map.put("code", 200);
			map.put("read", readCount);
			
		} catch (Exception e) {
			e.printStackTrace();
			
			map.put("code", 500);
			map.put("read", 0);
		}

		
		Gson gson = new Gson();
		String jsonStr = gson.toJson(map);
		
		response.getWriter().println(jsonStr);
	}
}
