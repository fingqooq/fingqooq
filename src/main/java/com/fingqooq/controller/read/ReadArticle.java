package com.fingqooq.controller.read;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.read.ReadArticleModel;
import com.google.gson.Gson;

public class ReadArticle extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int articleId = Integer.parseInt(request.getParameter("article-id"));
		HttpSession session = request.getSession(false);
		
		int userId = (int)session.getAttribute("user_id");
		
		ReadArticleModel ram = new ReadArticleModel(getServletContext());
		ram.insertRead(articleId, userId);
		
		int readCount = ram.getRead(userId);
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("code", 200);
		map.put("read", readCount);
		
		Gson gson = new Gson();
		String jsonStr = gson.toJson(map);
		
		response.getWriter().println(jsonStr);
	}

}
