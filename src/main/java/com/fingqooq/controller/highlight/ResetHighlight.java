package com.fingqooq.controller.highlight;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.highlight.ResetHighlightModel;

public class ResetHighlight extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int articleId = Integer.parseInt(request.getParameter("article_id"));
		HttpSession session = request.getSession(false);
		int userID = (int)session.getAttribute("user_id");
		
		//
		ResetHighlightModel rhm = new ResetHighlightModel(getServletContext(), userID, articleId);
		rhm.resetHighlight();
		
		response.getWriter().println("SUCCESS");
	}
}
