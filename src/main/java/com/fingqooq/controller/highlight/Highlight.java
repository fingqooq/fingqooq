package com.fingqooq.controller.highlight;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.highlight.GetHighlightModel;
import com.fingqooq.model.highlight.HighlightData;
import com.fingqooq.model.highlight.HighlightInfo;
import com.fingqooq.model.highlight.ResetHighlightModel;
import com.fingqooq.model.highlight.SaveHighlightModel;
import com.fingqooq.model.login.CheckUserModel;
import com.fingqooq.model.qooq.DidQooqModel;
import com.fingqooq.model.qooq.QooQArticleModel;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class Highlight extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		int articleId = Integer.parseInt(request.getParameter("article_id"));
		HttpSession session = request.getSession(false);
		String fbId = (String)session.getAttribute("fb_id");
		CheckUserModel userModel = new CheckUserModel(fbId, getServletContext());
		int userId = userModel.getUserID();
		
		GetHighlightModel ghm = new GetHighlightModel(getServletContext());
		List<HighlightInfo> list = ghm.getHighlightInfo(articleId, userId);
		GsonBuilder gb = new GsonBuilder();
		gb.setExclusionStrategies(new ExclusionStrategy() {
			
			@Override
			public boolean shouldSkipField(FieldAttributes f) {
				if (f.getName().equals("userId")) {
					return true;
				}
				return false;
			}
			
			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}
		});
		Gson gson = gb.create();
		String jsonStr = gson.toJson(list);
		response.getWriter().println(jsonStr);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		BufferedReader br = request.getReader();
		JsonReader jr = new JsonReader(br);
		Gson gson = new Gson();
		HighlightData hi = gson.fromJson(jr, HighlightData.class);
		int articleId = hi.getArticleId();
		
		// get user id
		
		String responseStr = "FAIL";
		try {
			HttpSession session = request.getSession(false);
			int userId = (int) session.getAttribute("user_id");
			hi.setUserId(userId);
			
			
			
			// check user qooqed this article
			// if not, automatically qooq
			DidQooqModel dqm = new DidQooqModel(getServletContext(), userId, articleId);
			boolean didQooq = dqm.didQooq();
			if (didQooq == false) {
				QooQArticleModel qam = new QooQArticleModel(getServletContext(), userId, articleId);
				qam.insertQooQ();
			}
			
			// reset previous highlight
			ResetHighlightModel rsm = new ResetHighlightModel(getServletContext(), userId, articleId);
			rsm.resetHighlight();
			
			// save current highlight
			SaveHighlightModel shm = new SaveHighlightModel(getServletContext());
			boolean result = shm.saveHighlight(hi);
			
			if (result) {
				responseStr = "SAVE";
			}
		} catch (Exception e) {
			responseStr = "LOGIN FAIL";
		}

		
		response.getWriter().println(responseStr);
	}

}
