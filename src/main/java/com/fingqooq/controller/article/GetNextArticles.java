package com.fingqooq.controller.article;

import com.fingqooq.model.article.AllArticleModel;
import com.fingqooq.model.article.MainPageArticleModel;
import com.fingqooq.model.user.UserInfo;
import com.fingqooq.model.user.UserInfoModel;
import com.google.gson.Gson;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 * Created by TaekSoon on 06/29/14.
 */
public class GetNextArticles extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	resp.setCharacterEncoding("utf-8");
    	int currentPage = Integer.parseInt(req.getParameter("current-page"));
    	
    	ServletContext sc = getServletContext();
        MainPageArticleModel articlesDB = new MainPageArticleModel(sc);
        
        HttpSession session = req.getSession(false);
        int user_id = 0;
        
        if (session != null && session.getAttribute("fb_id") != null) {
        	user_id = (int)session.getAttribute("user_id");
        	String fb_id = (String)session.getAttribute("fb_id");
        	
        	req.setAttribute("myarticles", "/myarticles/" + fb_id);
        	req.setAttribute("session", true);
        }else{
        	req.setAttribute("session", false);
        }

        List articles = articlesDB.getNextArticlesList(currentPage, user_id);
        
        Gson gson = new Gson();
        String jsonStr = gson.toJson(articles);

        resp.getWriter().println(jsonStr);
    }
}
