package com.fingqooq.controller.article;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.article.AllArticleModel;
import com.fingqooq.model.article.MyArticlesModel;
import com.fingqooq.model.user.UserInfo;
import com.fingqooq.model.user.UserInfoModel;

public class GetMyArticles extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	String path = request.getPathInfo();
        String fb_id = path.substring(1);
    	
		ServletContext sc = getServletContext();
		
        HttpSession session = request.getSession(false);
        if ( session != null && session.getAttribute("fb_id") != null){
        	String myFBid = (String)session.getAttribute("fb_id");
        	request.setAttribute("myarticles", myFBid);
        	request.setAttribute("session", true);
        } else{
        	request.setAttribute("session", false);
        }
        	
        UserInfoModel user = new UserInfoModel(sc, fb_id);
        UserInfo userInfo = user.getUserInfo();
        int userID = userInfo.getUserID();
        
        MyArticlesModel db = new MyArticlesModel(sc, userID);
        List articles = db.getMyArticles();
        
        
    	request.setAttribute("profile",userInfo);
        request.setAttribute("articles",articles);

        String myArticles = "/myArticles.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(myArticles);
        dispatcher.forward(request, response);
	}
}
