package com.fingqooq.controller.article;

import com.fingqooq.model.article.AllArticleModel;
import com.fingqooq.model.article.RecommendModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by WooGenius on 5/19/14.
 */
public class GetArticles extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    	
    	ServletContext sc = getServletContext();
        AllArticleModel articlesDB = new AllArticleModel(sc);
        RecommendModel recommendDB = new RecommendModel(sc);
        
        HttpSession session = req.getSession(false);
        int user_id = 0;
        
        if (session != null && session.getAttribute("fb_id") != null){
        	user_id = (int)session.getAttribute("user_id");
        	String fb_id = (String)session.getAttribute("fb_id");
        	req.setAttribute("myarticles", "/myarticles/" + fb_id);
        	req.setAttribute("session", true);
        }else{
        	req.setAttribute("session", false);
        }

        List articles = articlesDB.getAllArticlesList(user_id);
        List recommend = recommendDB.getRecommendList(user_id);
        req.setAttribute("articles", articles);
        req.setAttribute("recommends", recommend);

        String articlesJsp = "/articles.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(articlesJsp);
        dispatcher.forward(req, resp);
    }
}
