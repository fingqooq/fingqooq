package com.fingqooq.controller.article;

import com.fingqooq.model.article.AllArticleModel;
import com.fingqooq.model.article.OneArticleModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.util.Map;

/**
 * Created by WooGenius on 5/19/14.
 */
public class GetArticle extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	String path = req.getPathInfo();
        int id = Integer.parseInt(path.substring(1));
        
        HttpSession session = req.getSession(false);
        String fbId = null;
        Integer userId = 0;
        if ( session != null && session.getAttribute("fb_id") != null) {
        	fbId = (String)session.getAttribute("fb_id");
        	req.setAttribute("session", true);
        	req.setAttribute("myarticles", "/myarticles/" + fbId);
        	userId = (Integer)session.getAttribute("user_id");
        }
        else{
        	req.setAttribute("session", false);
        }
        
        ServletContext sc = getServletContext();
        
        OneArticleModel db = new OneArticleModel(sc, id, userId);

        Map article = db.getArticle();
        
        req.setAttribute("article",article);

        String articlesJsp = "/article.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(articlesJsp);
        dispatcher.forward(req, resp);
    }
}
