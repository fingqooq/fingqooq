package com.fingqooq.controller.qooq;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.highlight.ResetHighlightModel;
import com.fingqooq.model.qooq.QooQArticleModel;
import com.fingqooq.model.qooq.QooQCountModel;
import com.fingqooq.model.qooq.UnqooqModel;
import com.fingqooq.model.user.UserInfo;
import com.fingqooq.model.user.UserInfoModel;
import com.google.gson.Gson;

/**
 * Servlet implementation class QooqArticle
 */
public class QooqArticle extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int articleId = Integer.parseInt(request.getParameter("id"));
		boolean qooq = Boolean.parseBoolean(request.getParameter("qooq"));
		ServletContext sc = getServletContext();
		
		HttpSession session = request.getSession(false);
		int userID = (int)session.getAttribute("user_id");
		
		if (qooq) {
			QooQArticleModel qam = new QooQArticleModel(sc, userID, articleId);
			qam.insertQooQ();
		} else {
			ResetHighlightModel rhm = new ResetHighlightModel(sc, articleId, userID);
			rhm.resetHighlight();
			
			UnqooqModel um = new UnqooqModel(sc, userID, articleId);
			um.unQooq();
		}
		
		QooQCountModel qcm = new QooQCountModel(sc, articleId);
		Integer qooqCount = qcm.getQooQCount();
		
		UserInfoModel uim = new UserInfoModel(sc, userID);
		UserInfo ui = uim.getUserInfo();
		
		Map<String, Integer> jsonMap = new HashMap<String, Integer>();
		if (qooqCount != null) {
			jsonMap.put("code", 200);
			jsonMap.put("QooQ", qooqCount);
			jsonMap.put("myQooQ", ui.getNumOfQooq());
		} else {
			jsonMap.put("code", 500);
		}
		
		Gson gson = new Gson();
		String jsonStr = gson.toJson(jsonMap);
		
		PrintWriter out = response.getWriter();
		out.write(jsonStr);
	}
}
