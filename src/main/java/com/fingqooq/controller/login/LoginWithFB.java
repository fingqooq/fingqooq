package com.fingqooq.controller.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.login.CheckUserModel;
import com.fingqooq.model.login.CreateUserModel;

import facebook4j.Facebook;
import facebook4j.FacebookFactory;



public class LoginWithFB extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Facebook facebook = new FacebookFactory().getInstance();
		String appId = "293935917432608";
		String appSecret = "db80d16cbcbbcec8fc5a2f75a446f2aa";
		facebook.setOAuthAppId(appId, appSecret);
        facebook.setOAuthPermissions("email, publish_actions, publish_stream, user_likes, friends_likes, read_stream");
		
		request.getSession().setAttribute("facebook", facebook);
		StringBuffer callbackURL = request.getRequestURL();
		int index = callbackURL.lastIndexOf("/");
		callbackURL.replace(index, callbackURL.length(), "").append("/callback");
		
		response.sendRedirect(facebook.getOAuthAuthorizationURL(callbackURL.toString()));
	}

}
