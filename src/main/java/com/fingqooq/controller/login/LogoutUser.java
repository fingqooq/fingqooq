package com.fingqooq.controller.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutUser extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String contextPath = request.getContextPath().toString();
        HttpSession session = request.getSession();
        session.invalidate();
        
        String indexPath = contextPath + "/articles";
        
        response.sendRedirect(indexPath);
	}

}
