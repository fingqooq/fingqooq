package com.fingqooq.controller.login;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.login.CheckUserModel;
import com.fingqooq.model.login.CreateUserModel;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.PictureSize;
import facebook4j.User;


/**
 * Servlet implementation class CallbackServlet
 */
public class CallbackServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext sc = getServletContext();
		Facebook facebook = (Facebook) request.getSession().getAttribute("facebook");
		String oauthCode = request.getParameter("code");
		
		try {
			
			facebook.getOAuthAccessToken(oauthCode);
			
			String fb_id = facebook.getId();
			String name = facebook.getName();
			String profileImg = "";
			String smallImg = "";
			
			try {
				URL getProfileImg = facebook.getPictureURL(PictureSize.square);
				smallImg = getProfileImg.toString() + "";
				String [] split = smallImg.split("/");
				
				if (split.length == 8){
					profileImg = split[0] + "//" + split[2] +"/" + 
							split[3] +"/" + split[4] +"/" + split[5] +"/" +"s100x100" + "/" + split[7];
				}else{
					profileImg = split[0] + "//" + split[2] +"/" + 
							split[3] +"/" + split[4] +"/" +"s100x100" + "/" + split[6];
					System.out.println(profileImg);
				}
			} catch (Exception e) {
				URL getSmallProfileImg = facebook.getPictureURL(PictureSize.small);
				URL getProfileImg = facebook.getPictureURL(PictureSize.normal);
				
				smallImg = getSmallProfileImg.toString() + "";
				profileImg = getProfileImg.toString() + "";
			}
			
			User user = facebook.getUser(fb_id);
			String email = user.getEmail();
			String gender = user.getGender();
			
			if (fb_id != null) {
				CheckUserModel checkUser = new CheckUserModel(fb_id, sc);
				int userId = checkUser.getUserID();
				
				if ( userId == 0 ) {
					new CreateUserModel(sc, fb_id, email, name, profileImg, smallImg, gender);
					}
				HttpSession session = request.getSession();
				session.setAttribute("name", name);
				session.setAttribute("fb_id", fb_id);
				session.setAttribute("user_id", userId);	
				}
			
			} catch (FacebookException e) {
				response.getWriter().print(e);
				return;
			}
		response.sendRedirect(request.getContextPath() + "/");
	}
}
