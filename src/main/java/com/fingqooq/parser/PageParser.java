package com.fingqooq.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fingqooq.model.article.CheckArticleModel;
import com.fingqooq.model.qooq.QooQArticleModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

/**
 * Servlet implementation class ParsingPage
 */
public class PageParser extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BufferedReader br = request.getReader();
		JsonReader jr = new JsonReader(br);
		Type mapType = new TypeToken<Map<String, String>>(){}.getType();
		Gson gson = new Gson();
		Map<String, String> map = gson.fromJson(jr, mapType);
		
		ServletContext sc = getServletContext();
		HttpSession session = request.getSession(false);

		//세션이 비어있으면 로그인으로 가게 하기 
		String url = map.get("url");
		
		String pythonFileName = "/root/fingqooq/src/main/python/parser.py";
		String result = "";
		
		//return message
		Map<String, String> jsonMap = new HashMap<String, String>();
		Gson resultJson = new Gson();
		
		//세션체크
		try {
			if ( session == null )
			{
				jsonMap.put("code", "505");
				jsonMap.put("message", "session is empty");
			}else
			{
				//중복 확인
				int userId = (int) session.getAttribute("user_id");
				System.out.println(userId);
				CheckArticleModel checkArticle  = new CheckArticleModel(sc, url);
				if (checkArticle.isNotDuplication())
				{
					String userIdStr = String.valueOf(userId);
					Process p = Runtime.getRuntime().exec(new String[] {"python", pythonFileName, url, userIdStr});
				}else
				{
					QooQArticleModel qooq = new QooQArticleModel(sc, userId, url);
					qooq.insertQooQ();
				}
				
				jsonMap.put("code", "200");
				jsonMap.put("message", "parsing saved");
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonMap.put("code", "500");
			jsonMap.put("message", "server error");
		}
				

		String responseString = resultJson.toJson(jsonMap);
		PrintWriter out = response.getWriter();
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.write(responseString);
		
	}

}
