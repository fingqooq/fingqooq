package com.fingqooq.context;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.dbcp.BasicDataSource;

public class ServletContextLi implements ServletContextListener {
	
	BasicDataSource bds;
	
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		bds = new BasicDataSource();
		bds.setDriverClassName("com.mysql.jdbc.Driver");
		
		String addr = context.getInitParameter("dbAddr");
		String user = context.getInitParameter("dbUser");
		String pwd = context.getInitParameter("dbPw");
		
		bds.setUsername(user);
		bds.setPassword(pwd);
		bds.setUrl(addr);
		
		// Setting for idle
		bds.setTestWhileIdle(true);
		bds.setTestOnBorrow(true);
		bds.setTestOnReturn(false);
		bds.setValidationQuery("SELECT 1");
		
		context.setAttribute("DataSource", bds);
	}

	public void contextDestroyed(ServletContextEvent sce) {
		try {
			bds.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
