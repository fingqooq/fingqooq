package com.fingqooq.model.article;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class MainPageArticleModel {
	private static int PAGING_SIZE = 20;
	private ServletContext sc;
	
	public MainPageArticleModel(ServletContext sc) {
		this.sc = sc;
	}
	
    public List getNextArticlesList(final int currentPage, final int userId) {
    	// query for id, title
    	
		final String getNextArticleQuery = "select t2.aid as aid_num, t2.title as title, t4.qooq_count, IFNULL(t5.read_count, 0) as read_count "
				+ " from qooq t1 right join article t2 on t1.article_aid = t2.aid "
				+ " left join (select t3.article_aid, count(*) as qooq_count from qooq t3 group by t3.article_aid) t4 on t4.article_aid = t1.article_aid "
				+ " left join (select t5.article_aid, count(*) as read_count from read_article t5 group by t5.article_aid) t5 "
				+ " on t5.article_aid = t1.article_aid group by aid_num order by aid_num desc LIMIT ?, ?";
		final String didQooqQuery = "SELECT COUNT(*) AS did_qooq FROM qooq WHERE article_aid=? AND user_uid=?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				ResultSet rsDidQooq = null;
				PreparedStatement pstmtNext = null;
				PreparedStatement pstmt = null;
				PreparedStatement pstmtQooqCount = null;
				
				// object for json result
				List<Map<String, Object>> jsonList = new ArrayList<Map<String, Object>>();
				try {
					pstmtNext = conn.prepareStatement(getNextArticleQuery);
					pstmtNext.setInt(1, currentPage * PAGING_SIZE);
					pstmtNext.setInt(2, PAGING_SIZE);
					rs = pstmtNext.executeQuery();
					
					while (rs.next()) {
						Map<String, Object> jsonMap = new HashMap<String, Object>();
						int id = rs.getInt("aid_num");
						String title = rs.getString("title");
						int read = rs.getInt("read_count");
						int qooq = rs.getInt("qooq_count");
						
						if (userId != 0) {
							pstmt = conn.prepareStatement(didQooqQuery);
							pstmt.setInt(1, id);
							pstmt.setInt(2, userId);
							
							rsDidQooq = pstmt.executeQuery();
							rsDidQooq.next();
							jsonMap.put("didQooq", rsDidQooq.getInt("did_qooq"));
							
							
						}
						jsonMap.put("qooq", qooq);
						jsonMap.put("id", id);
						jsonMap.put("title", title);
						jsonMap.put("read", read);
						jsonList.add(jsonMap);
					}
					
					
					
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmtNext != null) { 
						try {
							pstmtNext.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmtQooqCount != null) { 
						try {
							pstmtQooqCount.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					return jsonList;
				}
			}
		};
		
		return (List)qt.getResult();
	}
}
