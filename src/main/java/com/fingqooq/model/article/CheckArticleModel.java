package com.fingqooq.model.article;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class CheckArticleModel {
	
	private final String url;
	private ServletContext sc;
	
	public CheckArticleModel(ServletContext sc, String url) {
		this.sc = sc;
		this.url = url;
	}
	
	public boolean isNotDuplication(){
		final String isDuplicationQuery ="select count(aid) as duplication_num from article where url = ?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				boolean result = true;
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				try{
					pstmt = conn.prepareStatement(isDuplicationQuery);
					pstmt.setString(1, url);
					rs = pstmt.executeQuery();
					
					rs.next();
					int resultNum = rs.getInt("duplication_num");

					if (resultNum > 0)
						result = false;
					
				}catch (SQLException e){
					e.printStackTrace();
				}finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				
				return result;
			}
		};
		
		return (Boolean)qt.getResult();
		
	}

}
