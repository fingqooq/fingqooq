package com.fingqooq.model.article;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

/*
 * DB Controller 내에서도 기능을 분리할것
 */
public class OneArticleModel {
	
	private ServletContext sc;
	private final int articleId;
	private final Integer userId;
	
	public OneArticleModel(ServletContext sc, int articleId, Integer userId) {
		this.sc = sc;
		this.articleId = articleId;
		this.userId = userId;
	}

	public Map getArticle() {
		final String getTagsQuery = "SELECT C2.name FROM article_category C1"
				+ " LEFT OUTER JOIN category C2 ON C1.cid = C2.cid"
				+ " WHERE C1.aid = ?";
		
		final String getArticleQuery = "SELECT aid, title, url, contents, (SELECT COUNT(*) FROM read_article WHERE user_uid = ?) AS read_count, titleImg FROM article "
									 + "WHERE aid = ?";
		final String getQooqQuery = "SELECT T2.uid, T2.name, T2.profileImg FROM qooq T1"
							+ " LEFT OUTER JOIN user T2 ON T2.uid = T1.user_uid"
						+ " WHERE T1.article_aid = ?";
		
		final String getDidReadQuery = "SELECT COUNT(*) AS NUM_READ FROM read_article WHERE user_uid = ? AND article_aid = ?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				PreparedStatement pstmtArticle = null;
				PreparedStatement pstmtTag = null;
				PreparedStatement pstmtQooq = null;
				PreparedStatement pstmtDidRead = null;
				
				ResultSet rsArticle = null;
				ResultSet rsTag = null;
				ResultSet rsQooq = null;
				ResultSet rsDidRead = null;
				
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				
				try {
					pstmtArticle = conn.prepareStatement(getArticleQuery);
					pstmtArticle.setInt(1, userId);
					pstmtArticle.setInt(2, articleId);
					rsArticle = pstmtArticle.executeQuery();
					// json for response
					while (rsArticle.next()) {
						String title = rsArticle.getString("title");
						String url = rsArticle.getString("url");
						String contents = rsArticle.getString("contents");
						int readCount = rsArticle.getInt("read_count");
						String titleImg = rsArticle.getString("titleImg");
						
						pstmtTag = conn.prepareStatement(getTagsQuery);
						pstmtQooq = conn.prepareStatement(getQooqQuery);
						
						pstmtTag.setInt(1, articleId);
						rsTag = pstmtTag.executeQuery();
						List<String> tag = new ArrayList<String>();
						while (rsTag.next()) {
							String tagName = rsTag.getString("name");
							tag.add(tagName);
						}
						
						pstmtQooq.setInt(1, articleId);
						rsQooq = pstmtQooq.executeQuery();
						List<Map<String, String>> qooq = new ArrayList<Map<String, String>>();
						Map<String, String> qooqMap = new HashMap<String, String>();
						while (rsQooq.next()) {
							int userId = rsQooq.getInt("uid");
							String userName = rsQooq.getString("name");
							String profileImg = rsQooq.getString("profileImg");
							
							qooqMap.put("id", ((Integer)userId).toString());
							qooqMap.put("name", userName);
							qooqMap.put("profileImg", profileImg);
							qooq.add(qooqMap);
						}
						
						pstmtDidRead = conn.prepareStatement(getDidReadQuery);
						pstmtDidRead.setInt(1, userId);
						pstmtDidRead.setInt(2, articleId);
						if (userId != null) {
							rsDidRead = pstmtDidRead.executeQuery();
							if (rsDidRead.next()) {
								int didRead = rsDidRead.getInt("NUM_READ");
								jsonMap.put("didRead", didRead);
							}
						}
						
						jsonMap.put("id", (Integer)articleId);
						jsonMap.put("title", title);
						jsonMap.put("titleImg", titleImg);
						jsonMap.put("url", url);
						jsonMap.put("contents", contents);
						jsonMap.put("tag", tag);
						jsonMap.put("QOOQ", qooq);
						jsonMap.put("read", (Integer)readCount);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (pstmtArticle != null) { 
						try {
							pstmtArticle.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmtQooq != null) { 
						try {
							pstmtQooq.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmtTag != null) { 
						try {
							pstmtTag.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (rsArticle != null) { 
						try {
							rsArticle.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (rsQooq != null) { 
						try {
							rsQooq.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					if (rsTag != null) { 
						try {
							rsTag.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				
				return jsonMap;
			}
		};
		
		return (Map)qt.getResult();
	}
}
