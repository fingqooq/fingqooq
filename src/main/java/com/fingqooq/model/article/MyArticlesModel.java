package com.fingqooq.model.article;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;
import com.fingqooq.model.qooq.QooQCountModel;

public class MyArticlesModel {
	private ServletContext sc;
	private int userID;
	
	public MyArticlesModel(ServletContext sc, int userID){
		this.sc = sc;
		this.userID = userID;
	}
	
	public List getMyArticles(){
		
		final String getUserArticles = "select t2.aid as aid_num, t2.title as title, t4.qooq_count, IFNULL(t5.read_count, 0) as read_count "
				+ " from qooq t1 inner join article t2 on t1.article_aid = t2.aid "
				+ " inner join (select t3.article_aid, count(*) as qooq_count from qooq t3 group by t3.article_aid) t4 on t4.article_aid = t1.article_aid "
				+ " left join (select t5.article_aid, count(*) as read_count from read_article t5 group by t5.article_aid) t5 "
				+ " on t5.article_aid = t1.article_aid where t1.user_uid = ? group by aid_num order by aid_num desc";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				
				// object for json result
				List<Map<String, Object>> jsonList = new ArrayList<Map<String, Object>>();
				try {
					pstmt = conn.prepareStatement(getUserArticles);
					pstmt.setInt(1, userID);
					rs = pstmt.executeQuery();
					
					
					while (rs.next()) {
						Map<String, Object> jsonMap = new HashMap<String, Object>();
						int id = rs.getInt("aid_num");
						String title = rs.getString("title");
						int read = rs.getInt("read_count");
						int qooq = rs.getInt("qooq_count");

						jsonMap.put("id", id);
						jsonMap.put("title", title);
						jsonMap.put("read", read);
						jsonMap.put("qooq", qooq);
						jsonList.add(jsonMap);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return jsonList;
			}
		};
				
		return (List)qt.getResult();
		
	}
	

}
