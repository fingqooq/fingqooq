package com.fingqooq.model.article;

import com.fingqooq.model.QueryTemplate;

import javax.servlet.ServletContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllArticleModel {
	
	private static final int PAGING_SIZE = 20;
	
	private ServletContext sc;
	
	public AllArticleModel(ServletContext sc) {
		this.sc = sc;
	}
	
    public List getAllArticlesList(int getUserID) {
    	// query for id, title
    	final int userID = getUserID;
    	
		
		final String getAllArticleQuery = "select t2.aid as aid_num, t2.title as title, t4.qooq_count, IFNULL(t5.read_count, 0) as read_count "
				+ " from qooq t1 right join article t2 on t1.article_aid = t2.aid "
				+ " left join (select t3.article_aid, count(*) as qooq_count from qooq t3 group by t3.article_aid) t4 on t4.article_aid = t1.article_aid "
				+ " left join (select t5.article_aid, count(*) as read_count from read_article t5 group by t5.article_aid) t5 "
				+ " on t5.article_aid = t1.article_aid group by aid_num order by aid_num desc LIMIT 0, ?";
		
		
		final String didQooqQuery = "SELECT COUNT(*) AS did_qooq FROM qooq WHERE article_aid=? AND user_uid=?";

        QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				ResultSet rsDidQooq = null;
				PreparedStatement pstmtAll = null;
				PreparedStatement pstmt = null;
				
				// object for json result
				List<Map<String, Object>> jsonList = new ArrayList<Map<String, Object>>();
				try {
					pstmtAll = conn.prepareStatement(getAllArticleQuery);
					pstmtAll.setInt(1, PAGING_SIZE);
					rs = pstmtAll.executeQuery();
					
					while (rs.next()) {
						Map<String, Object> jsonMap = new HashMap<String, Object>();
						int id = rs.getInt("aid_num");
						String title = rs.getString("title");
						int read = rs.getInt("read_count");
						int qooq = rs.getInt("qooq_count");
						
						if (userID != 0) {
							pstmt = conn.prepareStatement(didQooqQuery);
							pstmt.setInt(1, id);
							pstmt.setInt(2, userID);
							
							rsDidQooq = pstmt.executeQuery();
							rsDidQooq.next();
							jsonMap.put("didQooq", rsDidQooq.getInt("did_qooq"));
						}
						jsonMap.put("qooq",qooq);
						jsonMap.put("id", id);
						jsonMap.put("title", title);
						jsonMap.put("read", read);
						jsonList.add(jsonMap);
					}
					
					
					
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmtAll != null) { 
						try {
							pstmtAll.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return jsonList;
			}
		};
		
		return (List)qt.getResult();
	}
}
