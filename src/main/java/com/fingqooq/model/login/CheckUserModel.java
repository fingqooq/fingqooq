package com.fingqooq.model.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class CheckUserModel {
	
	private final String fb_id;
	private ServletContext sc;
	
	public CheckUserModel(String fb_id, ServletContext sc) {
		this.fb_id = fb_id;
		this.sc = sc;
	}

	public int getUserID() {
		final String isUserQuery = "SELECT uid FROM user where fb_id = ?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				int userId = 0;
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				try{
					pstmt = conn.prepareStatement(isUserQuery);
					pstmt.setString(1, fb_id);
					rs = pstmt.executeQuery();
					
					
					if (rs.next()){
						userId = rs.getInt("uid");
					}
					
				}catch (SQLException e){
					e.printStackTrace();
				}finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return userId;
			}
		};
		
		return (int)qt.getResult();
	}
}
