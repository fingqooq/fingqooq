package com.fingqooq.model.login;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class CreateUserModel {
	private ServletContext sc;
	private final String fb_id;
	private final String email;
	private final String name;
	private final String profileImg;
	private final String gender;
	private final String smallImg;
	
	public CreateUserModel(ServletContext sc, String fb_id, String email,
			String name, String profileImg, String smallImg, String gender){
		this.sc = sc;
		this.fb_id = fb_id;
		this.email = email;
		this.name = name;
		this.profileImg = profileImg;
		this.smallImg = smallImg;
		this.gender = gender;
		createUser();
	}

	private void createUser(){
		final String createUserQuery = "INSERT INTO user (fb_id, email, name, profileImg, small_img, gender) values (?, ?, ?, ?, ?, ?)";
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				try{
					pstmt = conn.prepareStatement(createUserQuery);
					pstmt.setString(1, fb_id);
					pstmt.setString(2, email);
					pstmt.setString(3, name);
					pstmt.setString(4, profileImg);
					pstmt.setString(5, smallImg);
					pstmt.setString(6, gender);
					pstmt.executeUpdate();
					
				}catch(SQLException e){
					e.printStackTrace();
				}finally{
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return null;
			}
		};
	}
	
	
}

