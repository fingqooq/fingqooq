package com.fingqooq.model.read;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class ReadArticleModel {
	private ServletContext sc;
	
	// article url
	public ReadArticleModel(ServletContext sc){
		this.sc = sc;
	}
	
	public void insertRead(final int articleId, final int userId){
		
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				CallableStatement cs = null;
				try {
					cs = conn.prepareCall("{CALL INSERT_READ(?, ?)}");
					cs.setInt(1, userId);
					cs.setInt(2, articleId);
					cs.executeUpdate();
					
				} catch(SQLException e) {
					e.printStackTrace();
				} finally {
					
					if (cs != null) { 
						try {
							cs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return null;
			}
		};
	}

	public Integer getRead(final int userId) {
		final String getReadCount = "SELECT COUNT(*) AS read_count FROM read_article WHERE user_uid = ?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				Integer readCount = null;
				try {
					pstmt = conn.prepareStatement(getReadCount);
					pstmt.setInt(1, userId);
					rs = pstmt.executeQuery();
					
					
					if (rs.next()) {
						readCount = rs.getInt("read_count");
					}
				} catch(SQLException e) {
					e.printStackTrace();
				} finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					return readCount;
				}
			}
		};
		
		return (Integer)qt.getResult();
	}

	public void deleteRead(final int articleId, final int userId) {
		final String deleteRead = "DELETE FROM read_article WHERE user_uid=? AND article_aid=?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				try {
					pstmt = conn.prepareStatement(deleteRead);
					pstmt.setInt(1, userId);
					pstmt.setInt(2, articleId);
					pstmt.executeUpdate();
				} catch(SQLException e) {
					e.printStackTrace();
				} finally {
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return null;
			}
		};
	}
	

}
