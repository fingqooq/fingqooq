package com.fingqooq.model.highlight;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class GetHighlightModel {
	
	private ServletContext sc;
	
	public GetHighlightModel(ServletContext sc) {
		this.sc = sc;
	}

	public List<HighlightInfo> getHighlightInfo(final int articleId, final int userId) {
		
		final List<HighlightInfo> list = new ArrayList<HighlightInfo>();
		
		final String getHighlightQuery = "SELECT aid, uid, start FROM highlight WHERE aid=? AND uid=? GROUP BY start";
		final String hiInfoQuery = "SELECT * FROM highlight WHERE aid=? AND uid=? AND start=?";
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				ResultSet rs1 = null;
				ResultSet rs2 = null;
				PreparedStatement pstmt1 = null;
				PreparedStatement pstmt2 = null;
				try{
					pstmt1 = conn.prepareStatement(getHighlightQuery);
					pstmt2 = conn.prepareStatement(hiInfoQuery);
					
					pstmt1.setInt(1, articleId);
					pstmt1.setInt(2, userId);
					
					pstmt2.setInt(1, articleId);
					pstmt2.setInt(2, userId);
					
					rs1 = pstmt1.executeQuery();
					while (rs1.next()) {
						HighlightInfo hi = new HighlightInfo();
						List<String> text = new ArrayList<String>();
						int start = rs1.getInt("start");
						pstmt2.setInt(3, start);
						rs2 = pstmt2.executeQuery();
						while (rs2.next()) {
							text.add(rs2.getString("text"));
							hi.setColor(rs2.getString("color"));
							hi.setStart(rs2.getInt("start"));
						}
						hi.setText(text);
						list.add(hi);
					}
				} catch(SQLException e) {
					e.printStackTrace();
				} finally {
					if (pstmt1 != null) { 
						try {
							pstmt1.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					return list;
				}
			}
		};
		
		return (List<HighlightInfo>)qt.getResult();
	}

	
}
