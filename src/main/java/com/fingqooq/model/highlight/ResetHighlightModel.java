package com.fingqooq.model.highlight;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class ResetHighlightModel {
	private ServletContext sc;
	private int articleId;
	private int userId;
	
	public ResetHighlightModel(ServletContext sc, int userId, int articleId) {
		this.sc = sc;
		this.articleId = articleId;
		this.userId = userId;
	}

	public void resetHighlight() {
		final String resetHighlightQuery = "DELETE FROM highlight WHERE aid=? AND uid= ?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				PreparedStatement pstmt = null;
				try{
					pstmt = conn.prepareStatement(resetHighlightQuery);
					pstmt.setInt(1, articleId);
					pstmt.setInt(2, userId);
					pstmt.executeUpdate();
					
				} catch(SQLException e) {
					e.printStackTrace();
				} finally {
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				
				return null;
			}
		};
	}
}
