package com.fingqooq.model.highlight;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class HighlightData {
	@SerializedName("article_id")
	private int articleId;
	private int userId;
	private List<HighlightInfo> data;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}
	
	public int getArticleId() {
		return articleId;
	}

	public List<HighlightInfo> getData() {
		return data;
	}
	
	@Override
	public String toString() {
		String ls = System.lineSeparator();
		StringBuilder sb = new StringBuilder();
		sb.append("Article Id : " + articleId + ls);
		sb.append("User Id : " + userId + ls);
		sb.append("Data : " + ls);
		for (HighlightInfo info : data) {
			sb.append(info.toString() + ls);
		}
		sb.append(ls);
		return sb.toString();
	}
}

