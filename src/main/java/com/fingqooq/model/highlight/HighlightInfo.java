package com.fingqooq.model.highlight;

import java.util.List;

public class HighlightInfo {
	private String color;
	private int start;
	private List<String> text;
	
	public void setColor(String color) {
		this.color = color;
	}

	public void setStart(int start) {
		this.start = start;
	}
	
	public String getColor() {
		return color;
	}

	public int getStart() {
		return start;
	}

	public List<String> getText() {
		return text;
	}
	
	public void setText(List<String> text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		String ls = System.lineSeparator();
		StringBuilder sb = new StringBuilder();
		sb.append("Color : " + color + ls);
		sb.append("Start : " + start + ls);
		sb.append("Text : " + ls);
		for (String content : text) {
			sb.append(content + ls);
		}
		sb.append(ls);
		return sb.toString();
	}
}
