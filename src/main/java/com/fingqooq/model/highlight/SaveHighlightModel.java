package com.fingqooq.model.highlight;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class SaveHighlightModel {
	private ServletContext sc;
	
	public SaveHighlightModel(ServletContext sc) {
		this.sc = sc;
	}
	
	public boolean saveHighlight(HighlightData info) {
		final int articleId = info.getArticleId();
		final int userId = info.getUserId();
		final List<HighlightInfo> data = info.getData();
		
		final String insertHighlightQuery = "INSERT INTO highlight (aid, uid, start, color, text, tidx) VALUES (?, ?, ?, ?, ?, ?)";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				PreparedStatement pstmtInsert = null;
				
				boolean result = false;
				try{
					conn.setAutoCommit(false);
					
					pstmtInsert = conn.prepareStatement(insertHighlightQuery);
					pstmtInsert.setInt(1, articleId);
					pstmtInsert.setInt(2, userId);
					for (HighlightInfo info : data) {
						pstmtInsert.setInt(3, info.getStart());
						pstmtInsert.setString(4, info.getColor());
						int i = 0;
						for (String content : info.getText()) {
							pstmtInsert.setString(5, content);
							pstmtInsert.setInt(6, i);
							
							pstmtInsert.executeUpdate();
							i++;
						}
					}
					result = true;
					conn.commit();
				} catch(SQLException e) {
					conn.rollback();
					e.printStackTrace();
				} finally {
					if (pstmtInsert != null) { 
						try {
							pstmtInsert.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					return result;
				}
			}
		};
		
		return (boolean)qt.getResult();
	}
}
