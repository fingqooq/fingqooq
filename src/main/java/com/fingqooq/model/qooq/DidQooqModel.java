package com.fingqooq.model.qooq;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class DidQooqModel {

	private ServletContext sc;
	private final int articleId;
	private final int userId;
	
	
	public DidQooqModel(ServletContext sc, int userId, int articleId) {
		this.sc = sc;
		this.articleId = articleId;
		this.userId = userId;
	}

	public boolean didQooq(){
		final String didQooqQuery = "SELECT COUNT(*) AS NUM_QOOQ FROM qooq WHERE user_uid=? AND article_aid=?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				boolean result = false;
				try{
					pstmt = conn.prepareStatement(didQooqQuery);
					pstmt.setInt(1, userId);
					pstmt.setInt(2, articleId);
					
					rs = pstmt.executeQuery();
					
					rs.next();
					if (rs.getInt("NUM_QOOQ") > 0) {
						result = true;
					}
				}catch(SQLException e){
					e.printStackTrace();
				}finally{
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					return result;
				}
			}
		};
		
		return (boolean)qt.getResult();
	}
	

}
