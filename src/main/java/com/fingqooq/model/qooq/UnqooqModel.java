package com.fingqooq.model.qooq;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class UnqooqModel {
	private ServletContext sc;
	private int userID;
	private int articleId;
	
	public UnqooqModel(ServletContext sc, int userID, int articleId){
		this.sc = sc;
		this.userID = userID;
		this.articleId = articleId;
	}
	
	public void unQooq() {
		final String unQooqQuery = "DELETE FROM qooq WHERE user_uid=? AND article_aid=?";
		
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				PreparedStatement pstmt = null;
				try {
					pstmt = conn.prepareStatement(unQooqQuery);
					pstmt.setInt(1, userID);
					pstmt.setInt(2, articleId);
					
					pstmt.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return null;
			}
		};
	}
}
