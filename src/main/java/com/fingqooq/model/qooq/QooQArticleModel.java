package com.fingqooq.model.qooq;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class QooQArticleModel {
	private ServletContext sc;
	private final int userID;
	private final String url;
	private final int articleID;
	
	// article url
	public QooQArticleModel(ServletContext sc, int userID, String url){
		this.sc = sc;
		this.userID = userID;
		this.url = url;
		this.articleID = 0;
	}
	
	//article id 
	public QooQArticleModel(ServletContext sc, int userID, int articleID){
		this.sc = sc;
		this.userID = userID;
		this.articleID = articleID;
		this.url = null;
	}
	
	public void insertQooQ(){
		QueryTemplate qt = new QueryTemplate(sc) {
			@Override
			public Object executeQuery() {
				CallableStatement cs = null;
				try{
					if ( url == null ){
						cs = conn.prepareCall("{CALL INSERT_QOOQ(?, ?)}");
						cs.setInt(1, userID);
						cs.setInt(2, articleID);
					}else{
						cs = conn.prepareCall("{CALL INSERT_QOOQ_URL(?, ?)}");
						cs.setInt(1, userID);
						cs.setString(2, url);
					}
					
					cs.executeUpdate();
					
				}catch(SQLException e){
					e.printStackTrace();
				}finally{
					if (cs != null) { 
						try {
							cs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
				return null;
			}
		};
	}
	

}
