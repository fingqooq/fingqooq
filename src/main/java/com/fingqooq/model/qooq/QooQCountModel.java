package com.fingqooq.model.qooq;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import com.fingqooq.model.QueryTemplate;

public class QooQCountModel {
	private ServletContext sc;
	private final int aid;
	
	
	public QooQCountModel(ServletContext sc, int aid){
		this.sc = sc;
		this.aid = aid;
	}
	
	public Integer getQooQCount(){
		final String getQooqQuery = "SELECT COUNT(*) AS qooq_count FROM qooq WHERE article_aid=?";
		QueryTemplate qt = new QueryTemplate(sc) {
			
			@Override
			public Object executeQuery() {
				ResultSet rs = null;
				PreparedStatement pstmt = null;
				Integer qooqCount = null;
				try{
					pstmt = conn.prepareStatement(getQooqQuery);
					pstmt.setInt(1, aid);
					rs = pstmt.executeQuery();
					
					rs.next();
					int result = rs.getInt("qooq_count"); 
					qooqCount =  result;
				}catch(SQLException e){
					e.printStackTrace();
				}finally{
					if (rs != null) { 
						try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (pstmt != null) { 
						try {
							pstmt.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					return qooqCount;
				}
			}
		};
		return (int)qt.getResult();
	}

}