package com.fingqooq.model.user;

import com.google.gson.annotations.SerializedName;

public class UserInfo {
	private String name;
	private String img;
	private int numOfQooq;
	private int numOfRead;
	private String myPage;
	private String fbId;
	private int user_id;
	
	public UserInfo(int user_id) {
		this.user_id = user_id;
	}
	
	public UserInfo(String fbId) {
		this.setFbId(fbId);
		setMyPage(fbId);
	}
	
	//name
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//img
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
	//numOfQooq & Read
	public int getNumOfQooq() {
		return numOfQooq;
	}
	public int getNumOfRead() {
		return numOfRead;
	}
	public void setNumOfQooq(int numOfQooq) {
		this.numOfQooq = numOfQooq;
	}
	public void setNumOfRead(int numOfRead) {
		this.numOfRead = numOfRead;
	}

	//set Mypage
	public String getMyPage() {
		return myPage;
	}
	
	public void setMyPage(String fbId) {
		this.myPage = "./myarticles/" + fbId;
	}

	//set&get User Id
	public void setUserID(int user_id) {
		this.user_id = user_id;
	}
	
	public int getUserID(){
		return this.user_id;
	}

	
	//set& get fbId
	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}
}
