package com.fingqooq.model.user;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import org.omg.IOP.ProfileIdHelper;

import com.fingqooq.model.QueryTemplate;

public class UserInfoModel {
	private ServletContext sc;
	private int user_id;
	private String fb_id;
	private Boolean isUserIdQuery;
	
	public UserInfoModel(ServletContext sc, int user_id) {
		this.sc = sc;
		this.user_id = user_id;
		this.isUserIdQuery = true;
	}
	public UserInfoModel(ServletContext sc, String fb_id) {
		this.sc = sc;
		this.fb_id = fb_id;
		this.isUserIdQuery = false;
	}
	
	public UserInfo getUserInfo() {
		final UserInfo userInfo;
		if (isUserIdQuery){
			userInfo = new UserInfo(user_id);
		}else{
			userInfo = new UserInfo(fb_id);
		}
		
		QueryTemplate qt = new QueryTemplate(sc) {
				
			@Override
			public Object executeQuery() {
				CallableStatement cs = null;
				ResultSet rs = null;
				boolean hasResult;
				
				try {
					if (isUserIdQuery){
						cs =  conn.prepareCall("{CALL GET_USER_INFO(?)}");
						cs.setInt(1, user_id);
					}else{
						cs =  conn.prepareCall("{CALL GET_USER_INFO_FB(?)}");
						cs.setString(1, fb_id);
					}
					
					hasResult = cs.execute();
					if (hasResult){
						rs = cs.getResultSet();
						rs.next();
						
						userInfo.setName(rs.getString("name"));
						userInfo.setImg(rs.getString("profileImg"));
						userInfo.setFbId(rs.getString("fb_id"));
						userInfo.setUserID(rs.getInt("uid"));
					}
					
					hasResult = cs.getMoreResults();
					if(hasResult){
						rs = cs.getResultSet();
						rs.next();
						userInfo.setNumOfQooq(rs.getInt("qooq_count"));
					}
					
					hasResult = cs.getMoreResults();
					if(hasResult){
						rs = cs.getResultSet();
						rs.next();
						userInfo.setNumOfRead(rs.getInt("read_count"));
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
					userInfo.setName("unknown_user");
					userInfo.setNumOfQooq(0);
					userInfo.setNumOfRead(0);
				} finally {
					if (cs != null) { 
						try {
							cs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
				}
				return userInfo;
			}
		};
		return (UserInfo)qt.getResult();
	}
}
