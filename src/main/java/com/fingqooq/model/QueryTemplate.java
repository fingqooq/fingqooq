package com.fingqooq.model;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

public abstract class QueryTemplate {
	
	protected DataSource ds;
	protected Connection conn;
	protected Object result;
	
	public QueryTemplate(ServletContext sc) {
		ds = (DataSource)sc.getAttribute("DataSource");
		try {
			conn = ds.getConnection();
			
			result = executeQuery();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Object getResult() {
		return result;
	}
	
	public abstract Object executeQuery();
}
